import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';
import { DetailsPage } from '../details/details';
import { CartPage } from '../cart/cart';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


/**
 * Generated class for the SubPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sub',
  templateUrl: 'sub.html',
})
export class SubPage {
  selectedData: any;
  loading: any;
  sizearray:any=[];
  colorarray:any=[];
  products: any = [];
  filtered: any = [];
  filteroption:boolean=false;;
  prods : any = [];
  filtercolor:any;
  filtersize:any;
  cat; sub;
  noproducts:boolean = false;;
  filterModal: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController, public productService: ProductProvider,
    ) {
    
    this.selectedData = navParams.get('data');
    console.log("GOT DATA: ", this.selectedData);
    this.cat = this.selectedData.category;
    this.sub = this.selectedData.subcategory;

    this.getProducts(this.cat, this.sub);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubPage');
  }

  getProducts(cat, sub) {

    // this.loading = this.loadingCtrl.create({
    //   spinner: 'ios',
    //   content: "Loading..."
    // });

    // this.loading.present();
    var filter=  {cat:this.cat,sub:this.sub}
    this.productService.getAllProducts(filter)
    .subscribe(
      data => {
        
        this.products = data.products;
        if(this.products.length ==0){
          this.noproducts = true;
        }
        console.log("SubPage: ", this.products);
        
       
        this.prods = this.products;
        // this.hideLoading();
      },
      error => {
        console.log("SubPage Error: ", error);
        // this.hideLoading();
      }
    );
  }

  Filter(call,tofilter,from){
    // this.loading = this.loadingCtrl.create({
    //   spinner: 'ios',
    //   content: "Loading..."
    // });

    // this.loading.present();
    
    
    
    
if (call == "filter"){
if (from == 1){

  this.filtercolor='&filter[pa_color]='+tofilter;
}
else if(from ==2)
{
  this.filtersize='&filter[pa_size]='+tofilter;
}
var filter=  {cat:this.cat,sub:this.sub,color:this.filtercolor,size:this.filtersize}
  this.productService.getAllProducts(filter)
  .subscribe(
      data => {
        
        this.products = data.products;
        console.log("SubPage: ", this.products);
      
     
        this.prods = this.products;
      // this.hideLoading();
      },
      error => {
        console.log("SubPage Error: ", error);
      // this.hideLoading();
      }
    );
  }
  else{
    this.filteroption = !this.filteroption;
    var self =this;
    this.productService.getAllAttirbutes().subscribe(
      data => {
        
        for(var i=0;i<data.length;i++){
          if(data[i].name == "Color"){
            this.productService.getattributedetails(data[i].id).subscribe(
              data => {
                self.colorarray = data;
              },
              error => {
                console.log("SubPage Error: ", error);
              // this.hideLoading();
              }
            );
          }
          else if(data[i].name == "Size"){
            this.productService.getattributedetails(data[i].id).subscribe(
              data => {
                self.sizearray = data;
                

              },
              error => {
                console.log("SubPage Error: ", error);
              // this.hideLoading();
              }
            );
          }
        }
        
      // this.hideLoading();
      },
      error => {
        console.log("SubPage Error: ", error);
      // this.hideLoading();
      }
    );
  
  }


  }
  Sort(){
    // this.loading = this.loadingCtrl.create({
    //   spinner: 'ios',
    //   content: "Loading..."
    // });

    // this.loading.present();

    this.productService.getAllProducts(this.cat)
    .subscribe(
      data => {
        
        this.products = data.products;
        console.log("SubPage: ", this.products);
        
       
        this.prods = this.products;
        // this.hideLoading();
      },
      error => {
        console.log("SubPage Error: ", error);
        // this.hideLoading();
      }
    );

  }
  private hideLoading() {
    this.loading.dismiss();
  }

  openDetails(product) {
    
    this.navCtrl.push(DetailsPage, {
      data: product,
    });
  }

  toCart() {    this.navCtrl.push(CartPage);  }

}

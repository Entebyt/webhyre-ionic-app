import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { ShippingPage } from '../shipping/shipping';
import {guest_checkout} from '../guest_checkout/guest_checkout' 
import { Storage } from '@ionic/storage';
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  items: any= [];
  total = 0;
  loading: any;
  show: boolean = false;
  DataObj: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public cartService: CartProvider, public loadingCtrl: LoadingController,
  private storage: Storage) {
    this.getCart();
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  getCart() {
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();
    this.storage.get('item').then((val) => {
      
      if(val.length == 0)
        {
          this.show = true;
         
        }
        else{
          this.items = val;
          this.getTotal(this.items);
        }
    
        });

    this.hideLoading();
  }

  getTotal(items) {
    debugger
    if(items.length !== 0){
      for(var i=0;i<items.length;i++){
        this.total += parseFloat(items[i].total)*parseFloat(items[i].quantity) 
      }
    
    }
    const getsum =(total, currentItem) => total + currentItem.price;
    // this.cartService.carttotal().subscribe((res) => {
    //   this.total=res.total;
    // });
  }

  private hideLoading() {
    this.loading.dismiss();
  }

  addItem(item) {
    //(item.quantity < 10) ? item.quantity=item.quantity+1 : 0
    if(item.quantity < 10) { 
      item.quantity=item.quantity+1;
      this.total = this.total + parseFloat(item.total);
    }
    this.storage.set('item',this.items)
   
  }

  reduceItem(item) {
    //(item.quantity >= 1) ? item.quantity=item.quantity-1 : 0
    if(item.quantity >= 1) { 
      item.quantity=item.quantity-1;
      this.total = this.total - parseFloat(item.total);
    }
    this.storage.set('item',this.items)
  }
  emptycart(){
    
    this.items =[];
    this.storage.set('item',this.items)
    this.show = true;
  }

  deleteItem(idx) {
    //before deleting reduce price
    this.total = this.total - parseFloat(this.items[idx].total) * parseFloat(this.items[idx].quantity);
    this.items.splice(idx, 1);
    this.storage.set('item',this.items)
  }

  toCheckout(items) {
    // also needs to check that items must not be empty
    if(this.items.length !== 0){
      this.DataObj.items = items;
      this.DataObj.total = this.total;
      
      this.navCtrl.push(guest_checkout, {
        data: this.DataObj
      });
    }

  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ImagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-images',
  templateUrl: 'images.html',
})
export class ImagesPage {
  images;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController) {
  this.images = navParams.get('data');
  console.log("Images: ", this.images);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImagesPage');
  }

  dismiss() {
    let returnMsg = 'Closed Images';
    this.viewCtrl.dismiss(returnMsg);
  }

}

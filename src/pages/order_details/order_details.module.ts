import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Order_Details } from './order_details';

@NgModule({
  declarations: [
    Order_Details,
  ],
  imports: [
    IonicPageModule.forChild(Order_Details),
  ],
})
export class Order_DetailsModule {}

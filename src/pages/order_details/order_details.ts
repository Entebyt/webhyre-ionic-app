import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'order-details',
  templateUrl: 'order_details.html',
})
export class Order_Details {
  items: any;
  loading: any;
  receivedData: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, 
  public cartService: CartProvider, public loadingCtrl: LoadingController) {
    this.receivedData = navParams.get('data');
 
  }
alert(view){
  if(view == 1){
    alert("Return Accepted")
  }
  else{
    alert("Cancel is accepted")
  }
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
  }

  getData() {
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();

    this.cartService.getCart()
    .subscribe((res) => {
      this.items = res;
      console.log("CART", this.items);
      
    });

    this.hideLoading();
  }

  private hideLoading() {
    this.loading.dismiss();
  }

}

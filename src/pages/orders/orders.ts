import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { OrderProvider } from '../../providers/orders/orders';
import {Order_Details} from '../order_details/order_details'
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  items: any;
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  public orderService: OrderProvider, public loadingCtrl: LoadingController,
  public storage: Storage) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
  }
  OrderDetails(item){
    debugger
    // this.orderService.ViewOrder(item.id).subscribe((res) => {
    //   debugger
    // });
     this.navCtrl.push(Order_Details,{
      data: item
    });
  }
  getData() {
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();
    debugger
    this.storage.get('userdetails').then((val) =>{
      if(val !== '' && val !== null){
        debugger
        this.orderService.getOrders(val.id)
        .subscribe((res) => {
          this.items = res.orders;
          console.log("CART", this.items);
          
        });
      }
      else{
        this.navCtrl.pop().then(() => { this.navCtrl.push(LoginPage)});
      }
    })


    this.hideLoading();
  }

  private hideLoading() {
    this.loading.dismiss();
  }

}

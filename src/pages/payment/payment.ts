import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CheckoutPage } from '../checkout/checkout';
import { OrderProvider } from '../../providers/orders/orders';
import { Storage } from '@ionic/storage';
import { CodmsgPage } from '../codmsg/codmsg';
import { HomePage } from '../home/home';
declare var RazorpayCheckout: any;
/**
 * 5% GST if the taxable value of the goods does not exceed Rs.1000 per piece.
 * All types of apparel and clothing of sale value exceeding Rs. 1000 per piece
 *  would be taxed at 12% GST
 */
  
@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  recievedData: any = {};
  total = 0;
  gst = 0;
  finalPrice = 0;
  deliveryCharges = 0;
  cartorder:any=[];
  loading: any;
  delDate;
  payment_method;
  customer_id:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public loadingCtrl: LoadingController,public orderService: OrderProvider,
  public storage: Storage) {
    this.recievedData = navParams.get('data');

    console.log("RECIEVED DATA: ", this.recievedData);

    this.total = this.recievedData.order_details.total;
    this.getFinalPrice(this.total);
    this.storage.get('cartorder').then((val) => {
      this.cartorder =val;
    })
    this.storage.get('userdetails').then((val) => {
      this.customer_id = val.id
    })
  }

  toNext() {
    // var options = {
    //   description: 'Credits towards consultation',
    //   image: 'https://i.imgur.com/3g7nmJC.png',
    //   currency: 'INR',
    //   key: 'rzp_test_1DP5mmOlF5G5ag',
    //   order_id: 'order_7HtFNLS98dSj8x',
    //   amount: '5000',
    //   name: 'foo',
    //   prefill: {
    //     email: 'pranav@razorpay.com',
    //     contact: '8879524924',
    //     name: 'Pranav Gupta'
    //   },
    //   theme: {
    //     color: '#F37254'
    //   }
    // }
     
    // var successCallback = function(success) {
    //   alert('payment_id: ' + success.razorpay_payment_id)
    //   var orderId = success.razorpay_order_id
    //   var signature = success.razorpay_signature
    // }
     
    // var cancelCallback = function(error) {
    //   alert(error.description + ' (Error '+error.code+')')
    // }
     
    // RazorpayCheckout.open(options, successCallback, cancelCallback)
  this.order();

  //
  // setTimeout(() => {
    
  // }, 10000);
  // this.navCtrl.push(HomePage);
  // this.order();
  }





  order(){
    var order =
    {
      "order": {
        "payment_details": {
          "method_id": "bacs",
          "method_title": "Direct Bank Transfer",
          "paid": true
        },
        "billing_address": {
          "first_name": "John",
          "last_name": "Doe",
          "address_1": "969 Market",
          "address_2": "",
          "city": "San Francisco",
          "state": "CA",
          "postcode": "94103",
          "country": "US",
          "email": "john.doe@example.com",
          "phone": "(555) 555-5555"
        },
        "shipping_address": 
          this.recievedData.shipping_details.address
        ,
        "customer_id": this.customer_id,
        "line_items": this.cartorder
      ,"shipping_lines":[
       {
         "method_id":"flat_rate",
         "method_title":"Flat Rate",
         "total":10
       } 
      ]
      }
    }
    var dataObj = {
      price: this.finalPrice,
      id: "OD123456789",
      // items: this.recievedData
    };
    this.orderService.createOrder(order)
    .subscribe((res) => {
     debugger
     this.navCtrl.push(CodmsgPage);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  getFinalPrice(total) {
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();

    if(total <= 1000) {
      this.gst = 0.05 * total;
    } else {
      this.gst = 0.12 * total;
    }

    // free delivery for products above 500 ruppees
    if(total > 500) {
      this.finalPrice = this.gst + total;
    } else {
      this.deliveryCharges = 100;
      this.finalPrice = this.gst + this.deliveryCharges + this.recievedData.order_details.total;
    }

    // setting delivery date i.e. after 5 days
    this.delDate = new Date();
    this.delDate.setDate(this.delDate.getDate() + 5);
    console.log("Delivery Date: ", this.delDate);

    this.hideLoading();
  }

  private hideLoading() {
    this.loading.dismiss();
  }

}

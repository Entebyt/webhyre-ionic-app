import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ShippingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shipping',
  templateUrl: 'shipping.html',
})
export class ShippingPage {

  address: any = {};
  items: any = {};
  total = 0;
  receivedData: any = {};
  DataObj: any = {};
  street:any;
  locality:any;
  city:any;
  pincode:any;
  state:any;

  addFlag : boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.receivedData = navParams.get('data');
  
    // this.items = this.receivedData.items;
    // this.total = this.receivedData.total;
    // this.DataObj.items = this.items;
    // this.DataObj.total = this.total;
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShippingPage');
  }

  //addShippingAddress(address)
  toNext(address) {
    console.log("ADDRESS: ", address);
    this.DataObj.address = address;
    console.log("DataObj: ", this.DataObj);
    var order=
    {
      shipping_details:this.DataObj,
      order_details:this.receivedData,
    }
  

    this.navCtrl.push(PaymentPage, {
      data: order,
    });
  }

  toggleFlag() {
    if(this.addFlag == true) { this.addFlag = false; }
    else { this.addFlag = true; }
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  dataRecieved: any = {};
  amount = 0;
  id: any;
  items: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.dataRecieved = navParams.get('data');
    this.amount = this.dataRecieved.price;
    this.items = this.dataRecieved.items;
    this.id = this.dataRecieved.id;
    
    console.log("DATA RECIEVED: ", this.dataRecieved);
    console.log("AMOUNT: " + this.amount);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

  toHome() {
    this.navCtrl.popToRoot();
  }
}

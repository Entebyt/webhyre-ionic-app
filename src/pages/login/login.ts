import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { ForgetPage } from '../forget/forget';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';
import { LoginProvider } from '../../providers/login/login'
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userdetails:any;
  user: any = {};
  toggleBtn: boolean = false;
  public typename: string = "password";

  constructor(private storage: Storage,public navCtrl: NavController, public navParams: NavParams,public signupservice:LoginProvider) {
  }

  toggleEye() {
    console.log("PREV: " + this.toggleBtn);
    if(this.toggleBtn === true){ 
      this.toggleBtn = false;   this.typename = "password"; }
    else { this.toggleBtn = true; this.typename = "text"; }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    console.log(this.user);

    this.signupservice.getlogin(2,this.user).subscribe((res) => {
      debugger
      if(res.status == "ok"){
        this.userdetails={username: res.user.username,email:res.user.email,name:res.user.displayname,id:res.user.id}
        this.storage.set('userdetails',this.userdetails);
        this.navCtrl.pop().then(() => { this.navCtrl.push(ProfilePage)});
      }
      else{
        alert(res.error);
      }
    
      
    })
  }

  toForget() {
    this.navCtrl.push(ForgetPage);
  }

  toSignup() {
    this.navCtrl.push(SignupPage);
  }

}

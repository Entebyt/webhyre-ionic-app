import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';
import { CategoryPage } from '../category/category';
import { CartPage } from '../cart/cart';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  slideImages: Array<{title: string, url: any}>;
  categorys: Array<{title: string, key: string, url: any}>;
  showsearch:boolean = false;
  arrivals: any;
  items:any=[];
  constructor(public navCtrl: NavController, public productService: ProductProvider,private storage: Storage) {
    this.slideImages = [
      { title: "SHORTS OFFER", url: "./assets/imgs/slides/slid1.jpeg" },
      { title: "SHORTS OFFER", url: "./assets/imgs/slides/slid2.jpeg" },
      { title: "SHORTS OFFER", url: "./assets/imgs/slides/slid3.jpeg" },
    ];

    this.categorys = [
      { title: "MEN'S", key: "Men", url: "./assets/imgs/trends/men.jpg" },
      { title: "WOMEN'S", key: "Women", url: "./assets/imgs/trends/women.jpg" },
      { title: "BOY'S", key: "Boys", url: "./assets/imgs/trends/boys.jpg" },
      { title: "GIRL'S", key: "Girls", url: "./assets/imgs/trends/girl.jpg" },
    ];

    this.getArrivalsData();
    this.getCartItems();
  }
  Show(){
    
    this.showsearch = !this.showsearch;
  }
  getArrivalsData() {
    this.productService.getArrivals()
    .subscribe((res) => {
      this.arrivals = res;
      console.log("ARRIVALS", this.arrivals);
    });
  }

  toCategory(cat) {
    this.navCtrl.push(CategoryPage, {
      data: cat
    });
  }

  toCart() {    this.navCtrl.push(CartPage);  }
  getCartItems(){

    this.storage.get('item').then((val) => {
       this.items =val;
        });
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ShippingPage} from '../shipping/shipping'
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'guest_checkout',
  templateUrl: 'guest_checkout.html',
})
export class guest_checkout {
  user: any = {};
  toggleBtn: boolean = false;
  public typename: string = "password";
  items: any = {};
  total = 0;
  receivedData: any = {};
  DataObj: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private storage: Storage) 
  {

    this.receivedData = navParams.get('data');
    this.items = this.receivedData.items;
    this.total = this.receivedData.total;
    this.DataObj.items = this.items;
    this.DataObj.total = this.total;
    storage.get('userdetails').then((val) =>{
      if(val !== '' && val !== null){
        navCtrl.pop();
        navCtrl.push(ShippingPage, {
          data: this.DataObj
        })
       
      }
    })
  }


  toggleEye() {
    console.log("PREV: " + this.toggleBtn);
    if(this.toggleBtn === true){ 
      this.toggleBtn = false;   this.typename = "password"; }
    else { this.toggleBtn = true; this.typename = "text"; }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login()  {
    this.navCtrl.push(LoginPage, {
      data: this.DataObj
    });

  }
  guest() {
  
      this.navCtrl.push(ShippingPage, {
        data: this.DataObj
      });

  

  }


}

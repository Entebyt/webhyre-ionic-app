import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { guest_checkout } from './guest_checkout'

@NgModule({
  declarations: [
    guest_checkout,
  ],
  imports: [
    IonicPageModule.forChild(guest_checkout),
  ],
})
export class guest_checkoutModule {}

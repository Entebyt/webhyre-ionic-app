import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ImagesPage } from '../images/images';
import { CartPage } from '../cart/cart';
import { CartProvider } from '../../providers/cart/cart';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import {ShippingPage} from '../shipping/shipping'
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  product: any ={};
  cat:any;
  plain_text:any;
  subcat:any;
  showprice:boolean = false;
  imgModal: any;
  discount:number;
  price1:any ='';
  price2:any= '';
  variationid:any='';
  Quantity:any;
  size:any=[];
  cart:any = [];
  placingcart:any = [];
  color:any=[];
  a:any;
  show: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public modalCtrl: ModalController,public cartService: CartProvider,
  private storage: Storage,public alertCtrl: AlertController) {
    storage.get('item').then((val) => {
      this.cart = val;
        });
    this.product = navParams.get('data');
    if(this.product.short_description !==""){
        this.show = true;
       this.plain_text = this.htmlToPlaintext(this.product.short_description);
       this.product.description =  this.htmlToPlaintext(this.product.description)
    }
    console.log("Product: ", this.product);
  }
  htmlToPlaintext(text) {
    return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }


  PriceUpdater(product){
    
    var last = product.attributes.length-1;
   if(product.attributes[last].selection){

    var self = this;
   this.a = product.variations.filter(function(e){
    var i = 0;
      if(e.attributes[i].option == ""){
        self.price1 = e.regular_price;
        self.price2 = e.sale_price;
        if(self.price1 == self.price2){
        }
        else{
          self.discount = ((parseInt(self.price1)-parseInt(self.price2))/parseInt(self.price1))*100;
          self.showprice =true;
        }
        return false;
       }

      while(i <= last){
        
        if( (e.attributes[i].option).toLowerCase() == (product.attributes[i].selection).toLowerCase()){
          
          if( i == last){
            self.price1 = e.regular_price;
            self.price2 = e.sale_price;
            self.variationid = e.id;
            if(self.price1 == self.price2){
            }
            else{
              self.discount = ((parseInt(self.price1)-parseInt(self.price2))/parseInt(self.price1))*100;
              self.showprice =true;
            }
            return true;
          }
          i++;
      }
      else{
        break;
      } 
     

       
     }
     
   })
   }
   console.log(this.a);
  }
  //presenting modal page
  toImages(imgs) {
    this.imgModal = this.modalCtrl.create(ImagesPage, {data: imgs});

    this.imgModal.onDidDismiss(data => {
      console.log(data);
    });

    this.imgModal.present();
  }
  toCart() {    this.navCtrl.push(CartPage);  }
  AddtoCart() {
    if(this.Quantity == undefined){
      this.showalert("Please Select Quantity")
    }
    else{
      debugger

    var carts = { total:this.product.price,
                  name:this.product.title,
                  thumnail:this.product.featured_src,
                  product_id:this.product.id,
                  quantity:this.Quantity,
                  variation_id:this.variationid
                }
    var placingorder = {
                        product_id:this.product.id,
                        quantity:this.Quantity,
                        variation_id:this.variationid
                      }
    this.storage.get('userdetails').then((val) => {
      console.log('Your user is', val);
     if(val !== "" && val !== null){

      // this.cartService.addProduct(this.product.id,this.Quantity).subscribe((res) => {
      //   cart =res;
      // });
      this.cart.push(carts);
      this.placingcart.push(placingorder);
      this.storage.set('cartorder',this.placingcart)
      this.storage.set('item',this.cart)
      this.navCtrl.push(CartPage,{
        data: carts
      })
     }
     else{
       this.navCtrl.push(LoginPage);
     }
    });
    }



  }
  showalert(title){
    let alert = this.alertCtrl.create({
      title: title,
      buttons: ['Dismiss']
    });
    alert.present();
  }
}

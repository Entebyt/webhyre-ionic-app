import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { LoginProvider } from '../../providers/login/login'
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})

export class SignupPage {
  user: any = {};
  toggleBtn: boolean = false;
  public typename: string = "password";
  selectOptions : { title: string, cssClass: string };

  constructor(public navCtrl: NavController, public navParams: NavParams,public signupservice:LoginProvider) {
    this.selectOptions = {
      cssClass: 'gender-select',
      title: 'Choose gender'
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  toggleEye() {
    console.log("PREV: " + this.toggleBtn);
    if(this.toggleBtn === true){ 
      this.toggleBtn = false;   this.typename = "password"; }
    else { this.toggleBtn = true; this.typename = "text"; }
  }

  register() {
    console.log("SignupUser: ", this.user);
    var dhruv:any;
    this.signupservice.getlogin(1,this.user).subscribe((res) => {
      debugger
    dhruv = res;
        this.navCtrl.pop().then(() => this.navCtrl.push(LoginPage));
    })

  }
}

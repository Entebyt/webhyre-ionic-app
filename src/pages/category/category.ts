import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MenuProvider } from '../../providers/menu/menu';
import { SubPage } from '../sub/sub';

/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  loading: any;
  data: any;
  filtered: any;
  cat;
  items: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  public dataService: MenuProvider, public loadingCtrl: LoadingController) {
    this.cat = navParams.get('data');

    this.getCategoryData(this.cat);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryPage');
  }

  getCategoryData(cat) {

    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });

    this.loading.present();

    this.dataService.getCategory()
    .subscribe(
      data => {
        this.data = data;
        console.log("ALL DATA: ", this.data);
        this.filtered = this.data.filter((c: any) => {
          return (c.category.toLowerCase() == this.cat.toLowerCase());
        });
        this.items = this.filtered[0].subs;
        console.log("FILTERED: ", this.filtered);
        console.log("ITEMS SUBS: ", Array.isArray(this.items));
        this.hideLoading();
      },
      error => {
        console.log("SubPage Error: ", error);
        this.hideLoading();
      }
    );
  }

  private hideLoading() {
    this.loading.dismiss();
  }

  openSub(subcat) {
    var selectedItem = {
      'category':  this.cat,
      'subcategory': subcat
    };

    console.log("SELECTED: ", selectedItem);
    
    this.navCtrl.push(SubPage, {
      data: selectedItem
    });
  }

}

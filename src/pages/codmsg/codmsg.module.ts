import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodmsgPage } from './codmsg';

@NgModule({
  declarations: [
    CodmsgPage,
  ],
  imports: [
    IonicPageModule.forChild(CodmsgPage),
  ],
})
export class CodmsgPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { ShippingPage } from '../shipping/shipping';
import {guest_checkout} from '../guest_checkout/guest_checkout' 
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CodmsgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-codmsg',
  templateUrl: 'codmsg.html',
})
export class CodmsgPage {

  recievedData: any;
  deliveryCharges: number;
  gst: any;
  finalPrice: any;
  delDate: Date;
  items: any= [];
  total = 0;
  loading: any;
  show: boolean = false;
  DataObj: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public cartService: CartProvider,private storage: Storage,public loadingCtrl: LoadingController) {
    this.getCart();
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  getCart() {

     // setting delivery date i.e. after 5 days
     this.delDate = new Date();
     this.delDate.setDate(this.delDate.getDate() + 5);
     // console.log("Delivery Date: ", this.delDate);
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();
    this.storage.get('item').then((val) => {
      
      if(val.length == 0)
        {
          this.show = true;
        this.getTotal(this.items); 
        }
        else{
          this.items = val;
          this.getTotal(this.items);
        }
    
        });

    this.hideLoading();
  }

  getTotal(items) {
    debugger
    if(items.length !== 0){
      for(var i=0;i<items.length;i++){
      this.total += parseFloat(items[i].total)*parseFloat(items[i].quantity) ;
    //  this.getFinalPrice(this.total);  


    if(this.total <= 1000) {
      this.gst = 0.05 * this.total;
    } else {
      this.gst = 0.12 * this.total;
    }

    // free delivery for products above 500 ruppees
    if(this.total > 500) {
      this.finalPrice = this.gst + this.total;
    } else {
      this.deliveryCharges = 100;
      this.finalPrice = this.gst + this.deliveryCharges + this.total;
    }

this.total=this.finalPrice;

      }
    
    }
    const getsum =(total, currentItem) => total + currentItem.price;
    // this.cartService.carttotal().subscribe((res) => {
    //   this.total=res.total;
    // });
  }



  getFinalPrice(total) {
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();

    if(total <= 1000) {
      this.gst = 0.05 * total;
    } else {
      this.gst = 0.12 * total;
    }

    // free delivery for products above 500 ruppees
    if(total > 500) {
      this.finalPrice = this.gst + total;
    } else {
      this.deliveryCharges = 100;
      this.finalPrice = this.gst + this.deliveryCharges + this.recievedData.order_details.total;
    }

    // setting delivery date i.e. after 5 days
    this.delDate = new Date();
    this.delDate.setDate(this.delDate.getDate() + 5);
    console.log("Delivery Date: ", this.delDate);

    this.hideLoading();
  }





  private hideLoading() {
    this.loading.dismiss();
  }





  ddte(){
   
    }
}














import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { ShippingPage } from '../shipping/shipping';
import {guest_checkout} from '../guest_checkout/guest_checkout' 
/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class WishlistPage {

  items: any;
  total = 0;
  loading: any;
  show: boolean = false;
  DataObj: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public cartService: CartProvider, public loadingCtrl: LoadingController) {
    this.getCart();
    debugger
    this.getTotal(this.items);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  getCart() {
    this.loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: "Loading..."
    });
    this.loading.present();

    this.cartService.getCart()
    .subscribe((res) => {
      if(res == "Cart is empty!")
      {
        this.show = true;
       
      }
      else{
        this.items = res;
      }
      console.log("CART", this.items);
      this.getTotal(this.items);
    });

    this.hideLoading();
  }

  getTotal(items) {
    // if(this.items ! == "Cart is empty!"){
    //   this.total = items.reduce((a,b) => b.price*b.quantity + (a.price ? a.price*a.quantity : a));
    //   console.log("Total " + this.total);
    // }
    //const getsum =(total, currentItem) => total + currentItem.price;
    this.cartService.carttotal().subscribe((res) => {
      this.total=res.total;
    });
  }

  private hideLoading() {
    this.loading.dismiss();
  }

  addItem(item) {
    //(item.quantity < 10) ? item.quantity=item.quantity+1 : 0
    if(item.quantity < 10) { 
      item.quantity=item.quantity+1;
      this.total = this.total + item.price;
    }  
  }

  reduceItem(item) {
    //(item.quantity >= 1) ? item.quantity=item.quantity-1 : 0
    if(item.quantity >= 1) { 
      item.quantity=item.quantity-1;
      this.total = this.total - item.price;
    }
  }
  emptycart(){
    debugger
    this.cartService.clearcart().subscribe((res) => {
     debugger
    });
  }

  deleteItem(idx) {
    //before deleting reduce price
    this.total = this.total - this.items[idx].price * this.items[idx].quantity;
    this.items.splice(idx, 1);
  }

  toCheckout(items) {
    // also needs to check that items must not be empty
    this.DataObj.items = items;
    this.DataObj.total = this.total;
    
    this.navCtrl.push(guest_checkout, {
      data: this.DataObj
    });
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user: any = {};
  user_edit: any = {};
  address1: any = {};
  address2: any = {};

  constructor(private storage: Storage,public navCtrl: NavController, public navParams: NavParams) {
    debugger
    storage.get('userdetails').then((val) => {
      console.log('Your user is', val);
      this.user = {
        firstname: val.name,
        email: val.email,
        mobile: 7011721626
      };
    });
   

    this.address1 = {
      street: "H-1, ABC",
      locality: "XYZ, EFG",
      city: "NCR",
      pincode: "200000",
      state: "UP"
    };

    this.address2 = {
      street: "H-2, XYZ",
      locality: "ABC, LMN",
      city: "DEL",
      pincode: "200200",
      state: "DEL"
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}

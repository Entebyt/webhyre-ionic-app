import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Order_Details } from '../pages/order_details/order_details';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenuProvider } from '../providers/menu/menu';
import { IonicStorageModule } from '@ionic/storage'
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { CartPage } from '../pages/cart/cart';
import { SubPage } from '../pages/sub/sub';
import { CategoryPage } from '../pages/category/category';
import { DetailsPage } from '../pages/details/details';
import { ImagesPage } from '../pages/images/images';
import { ProductProvider } from '../providers/product/product';
import { CartProvider } from '../providers/cart/cart';
import { LoginProvider } from '../providers/login/login';
import { OrderProvider } from '../providers/orders/orders';
import { ShippingPage } from '../pages/shipping/shipping';
import { PaymentPage } from '../pages/payment/payment';
import { CheckoutPage } from '../pages/checkout/checkout';
import { ReturnPage } from '../pages/return/return';
import { PrivacyPage } from '../pages/privacy/privacy';
import { OrdersPage } from '../pages/orders/orders';
import { TermsPage } from '../pages/terms/terms';
import { ProfilePage } from '../pages/profile/profile';
import { ForgetPage } from '../pages/forget/forget';
import { AboutPage } from '../pages/about/about';
import { CodmsgPage } from '../pages/codmsg/codmsg';
import{guest_checkout} from '../pages/guest_checkout/guest_checkout'
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    CartPage,
    SubPage,
    CategoryPage,
    DetailsPage,
    ImagesPage,
    ShippingPage,
    PaymentPage,
    CheckoutPage,
    ReturnPage,
    OrdersPage,
    PrivacyPage,
    TermsPage,
    ProfilePage,
    ForgetPage,
    AboutPage,
    guest_checkout,
    Order_Details,
    CodmsgPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    CartPage,
    SubPage,
    CategoryPage,
    DetailsPage,
    ImagesPage,
    ShippingPage,
    PaymentPage,
    CheckoutPage,
    ReturnPage,
    OrdersPage,
    PrivacyPage,
    TermsPage,
    ProfilePage,
    ForgetPage,
    AboutPage,
    guest_checkout,
    Order_Details,
    CodmsgPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MenuProvider,
    ProductProvider,
    CartProvider,
    LoginProvider,
    OrderProvider
  ]
})
export class AppModule {}

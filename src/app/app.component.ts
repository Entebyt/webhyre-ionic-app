import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {} from '../pages/guest_checkout/guest_checkout'
import { HomePage } from '../pages/home/home';
import { MenuProvider } from '../providers/menu/menu';
import { SubPage } from '../pages/sub/sub';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ReturnPage } from '../pages/return/return';
import { OrdersPage } from '../pages/orders/orders';
import { TermsPage } from '../pages/terms/terms';
import { PrivacyPage } from '../pages/privacy/privacy';
import { AboutPage } from '../pages/about/about';
import { Storage } from '@ionic/storage';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  activePage: any;
  home: any;
  pages: any;
  user:any;
  hide:boolean = false;
  pages2: any;
  loader: any;
  otherpages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, 
    public menuService: MenuProvider, loadingCtrl: LoadingController,private storage: Storage) {
    this.initializeApp();
    storage.get('userdetails').then((val) => {   
      console.log('Your user is', val);
     if(val !== "" && val !== null){
        this.user = val;
        this.hide = true;
     } 
    });
    this.otherpages = [
      { title: 'About Us', component: AboutPage, icon: 'ios-information-circle-outline'},
      { title: 'Orders', component: OrdersPage, icon: 'ios-home-outline' },
      { title: 'T&C', component: TermsPage, icon : 'ios-woman-outline' },
      { title: 'Return Policy', component: ReturnPage, icon : 'ios-man-outline' },
      { title: 'Privacy Policy', component: PrivacyPage, icon : 'ios-happy-outline' },      
    ];

    console.log("OTHER PAGES: ", this.otherpages);
    this.getMenuData();
    this.activePage = HomePage;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  getMenuData() {
    this.menuService.getMenu()
    .subscribe((res) => {
      this.pages = res;
      console.log("MENU", this.pages);
    });
  }

  toggleSection(i){
    // basic functionality i.e. toggling function
    this.pages[i].open = !this.pages[i].open;
  }

  toggleSub(i, j, subs) {
    //this.pages[i].subs[j].open = !this.pages[i].subs[j].open; 
  }

  openSub(i,j) {
    var selectedItem = {
      'category':  this.pages[i].category,
      'subcategory': this.pages[i].subs[j].subcategory
    };

    console.log("SELECTED: ", selectedItem);
    
    this.nav.push(SubPage, {
      data: selectedItem
    });
  }

  toLogin(call) { 
    
    if(call == 2){
       this.storage.set('userdetails',"")
    } 
  this.nav.push(LoginPage);  }
  toSignup() { this.nav.push(SignupPage);  }

  openPage(page) {
    this.nav.push(page.component);
    this.activePage = page;
    
  }

  checkActive(page) {
    return page == this.activePage;
  }
}


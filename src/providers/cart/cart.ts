import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CartProvider {
  myheader:any;
  constructor(public http: HttpClient) {
    this.myheader = new HttpHeaders();
    
    this.myheader.append('Content-Type','application/json');
    console.log('Hello CartProvider Provider');
  }

  getCart(): any {
    return this.http.get('https://www.undergarment.co.in/wp-json/wc/v2/cart')
    .map((res: any) => res);
  }
  addProduct(prodid,quantity):any{
    return this.http.post('https://www.undergarment.co.in/wp-json/wc/v2/cart/add',
      {
        "product_id": prodid,
        "variation": {
          "attribute_pa_color": "light",
          "attribute_pa_size": "m-medium"
        },
        "quantity": quantity
    }
    ,
      {headers:this.myheader}
    )
    .map((res: any) => res);
  }
  clearcart():any{
    return this.http.get('https://www.undergarment.co.in/wp-json/wc/v2/cart/clear')
    .map((res: any) => res);
  }
  carttotal():any{
    return this.http.get('https://www.undergarment.co.in/wp-json/wc/v2/cart/totals')
    .map((res: any) => res);
  }

}

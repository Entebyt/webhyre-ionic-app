import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductProvider {
  products: any = [];
  constructor(public http: HttpClient) {
    console.log('Hello ProductProvider Provider');
  }

  getArrivals(): any {
    return this.http.get('assets/data/arrivals-data.json')
    .map((res: any) => res);
  }

  getAllProducts(filter): any {
    if(filter.color || filter.size ){
      if(filter.color && filter.size){
        return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]='+filter.cat+filter.color+filter.size+'&filter[category]='+filter.sub+'&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map((res: any) => res);
      }
      else if(filter.color){
        return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]='+filter.cat+filter.color+'&filter[category]='+filter.sub+'&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map((res: any) => res);
      }
      else if(filter.size){
        return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]='+filter.cat+'&filter[category]='+filter.sub+filter.size+'&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map((res: any) => res);
      }
    }
    else{
      return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]='+filter.cat+'&filter[category]='+filter.sub+'&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map((res: any) => res);
    }
  }
  getAllAttirbutes(): any {
    return this.http.get('https://undergarment.co.in/wp-json/wc/v3/products/attributes?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27')
    .map((res: any) => res);
  }
  getattributedetails(id): any {
    return this.http.get('https://undergarment.co.in/wp-json/wc/v3/products/attributes/'+id+'/terms?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27')
    .map((res: any) => res);

  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HomePage } from '../../pages/home/home';

/*
  Generated class for the MenuProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MenuProvider {
  pages: Array<{title: string, component: any, icon: string}>;
  
  constructor(public http: HttpClient) {
    console.log('Hello MenuProvider Provider');
  }

  getMenu(): any {
    return this.http.get('assets/data/menu.json')
    .map((res: any) => res);
  }

  getCategory(): any {
    return this.http.get('assets/data/categories.json')
    .map((res: any) => res);
  }
  getMenu2(): any {
    this.pages = [
      { title: 'Orders', component: HomePage, icon: 'ios-filing-outline' },
      { title: 'T&C', component: HomePage, icon: 'ios-basket-outline' },
      { title: 'Return Policy', component: HomePage, icon: 'ios-heart-outline' }
    ];

  }


}

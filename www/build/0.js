webpackJsonp([0],{

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WishlistPageModule", function() { return WishlistPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wishlist__ = __webpack_require__(324);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WishlistPageModule = /** @class */ (function () {
    function WishlistPageModule() {
    }
    WishlistPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__wishlist__["a" /* WishlistPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__wishlist__["a" /* WishlistPage */]),
            ],
        })
    ], WishlistPageModule);
    return WishlistPageModule;
}());

//# sourceMappingURL=wishlist.module.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WishlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__guest_checkout_guest_checkout__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WishlistPage = /** @class */ (function () {
    function WishlistPage(navCtrl, navParams, cartService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cartService = cartService;
        this.loadingCtrl = loadingCtrl;
        this.total = 0;
        this.show = false;
        this.DataObj = {};
        this.getCart();
        debugger;
        this.getTotal(this.items);
    }
    WishlistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CartPage');
    };
    WishlistPage.prototype.getCart = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        this.cartService.getCart()
            .subscribe(function (res) {
            if (res == "Cart is empty!") {
                _this.show = true;
            }
            else {
                _this.items = res;
            }
            console.log("CART", _this.items);
            _this.getTotal(_this.items);
        });
        this.hideLoading();
    };
    WishlistPage.prototype.getTotal = function (items) {
        var _this = this;
        // if(this.items ! == "Cart is empty!"){
        //   this.total = items.reduce((a,b) => b.price*b.quantity + (a.price ? a.price*a.quantity : a));
        //   console.log("Total " + this.total);
        // }
        //const getsum =(total, currentItem) => total + currentItem.price;
        this.cartService.carttotal().subscribe(function (res) {
            _this.total = res.total;
        });
    };
    WishlistPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    WishlistPage.prototype.addItem = function (item) {
        //(item.quantity < 10) ? item.quantity=item.quantity+1 : 0
        if (item.quantity < 10) {
            item.quantity = item.quantity + 1;
            this.total = this.total + item.price;
        }
    };
    WishlistPage.prototype.reduceItem = function (item) {
        //(item.quantity >= 1) ? item.quantity=item.quantity-1 : 0
        if (item.quantity >= 1) {
            item.quantity = item.quantity - 1;
            this.total = this.total - item.price;
        }
    };
    WishlistPage.prototype.emptycart = function () {
        debugger;
        this.cartService.clearcart().subscribe(function (res) {
            debugger;
        });
    };
    WishlistPage.prototype.deleteItem = function (idx) {
        //before deleting reduce price
        this.total = this.total - this.items[idx].price * this.items[idx].quantity;
        this.items.splice(idx, 1);
    };
    WishlistPage.prototype.toCheckout = function (items) {
        // also needs to check that items must not be empty
        this.DataObj.items = items;
        this.DataObj.total = this.total;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__guest_checkout_guest_checkout__["a" /* guest_checkout */], {
            data: this.DataObj
        });
    };
    WishlistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cart',
            templateUrl: 'cart.html',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], WishlistPage);
    return WishlistPage;
}());

//# sourceMappingURL=wishlist.js.map

/***/ })

});
//# sourceMappingURL=0.js.map
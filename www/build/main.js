webpackJsonp([21],{

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return guest_checkout; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shipping_shipping__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var guest_checkout = /** @class */ (function () {
    function guest_checkout(navCtrl, navParams, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.user = {};
        this.toggleBtn = false;
        this.typename = "password";
        this.items = {};
        this.total = 0;
        this.receivedData = {};
        this.DataObj = {};
        this.receivedData = navParams.get('data');
        this.items = this.receivedData.items;
        this.total = this.receivedData.total;
        this.DataObj.items = this.items;
        this.DataObj.total = this.total;
        storage.get('userdetails').then(function (val) {
            if (val !== '' && val !== null) {
                navCtrl.pop();
                navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__shipping_shipping__["a" /* ShippingPage */], {
                    data: _this.DataObj
                });
            }
        });
    }
    guest_checkout.prototype.toggleEye = function () {
        console.log("PREV: " + this.toggleBtn);
        if (this.toggleBtn === true) {
            this.toggleBtn = false;
            this.typename = "password";
        }
        else {
            this.toggleBtn = true;
            this.typename = "text";
        }
    };
    guest_checkout.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    guest_checkout.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */], {
            data: this.DataObj
        });
    };
    guest_checkout.prototype.guest = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__shipping_shipping__["a" /* ShippingPage */], {
            data: this.DataObj
        });
    };
    guest_checkout = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'guest_checkout',template:/*ion-inline-start:"D:\Ionic app\src\pages\guest_checkout\guest_checkout.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Guest Checkout</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content id="page_bg">\n\n  <div id="title_bg">\n\n  <div text-center style="font-size: 25vw; margin: 5vw; margin-bottom: 0vw; padding-top: 5vw;">\n\n    <img src="assets/imgs/theme_logo.png" />  \n\n  </div>\n\n\n\n  </div>\n\n\n\n  <div padding>\n\n    <ion-row>\n\n      <ion-col>       \n\n\n\n    <button ion-button class="login-button" (click)="login()">Login</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n          <ion-col>       \n\n            \n\n        <button ion-button class="login-button" (click)="guest()">Guest Checkout</button>\n\n            </ion-col>\n\n          </ion-row>\n\n    </div>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Ionic app\src\pages\guest_checkout\guest_checkout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], guest_checkout);
    return guest_checkout;
}());

//# sourceMappingURL=guest_checkout.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShippingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_payment__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ShippingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ShippingPage = /** @class */ (function () {
    function ShippingPage(navCtrl, navParams, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.address = {};
        this.items = {};
        this.total = 0;
        this.receivedData = {};
        this.DataObj = {};
        this.addFlag = false;
        this.receivedData = navParams.get('data');
        // this.items = this.receivedData.items;
        // this.total = this.receivedData.total;
        // this.DataObj.items = this.items;
        // this.DataObj.total = this.total;
    }
    ShippingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShippingPage');
    };
    //addShippingAddress(address)
    ShippingPage.prototype.toNext = function (address) {
        console.log("ADDRESS: ", address);
        this.DataObj.address = address;
        console.log("DataObj: ", this.DataObj);
        var order = {
            shipping_details: this.DataObj,
            order_details: this.receivedData,
        };
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__payment_payment__["a" /* PaymentPage */], {
            data: order,
        });
    };
    ShippingPage.prototype.toggleFlag = function () {
        if (this.addFlag == true) {
            this.addFlag = false;
        }
        else {
            this.addFlag = true;
        }
    };
    ShippingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-shipping',template:/*ion-inline-start:"D:\Ionic app\src\pages\shipping\shipping.html"*/'<!--\n\n  Generated template for the ShippingPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Shipping Address</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <!-- <ion-card>\n\n    <ion-card-header>Address 1</ion-card-header>\n\n    <ion-card-content>\n\n      <p>Street: {{address1.street}}</p>\n\n      <p>Locality: {{address1.locality}}</p>\n\n      <p>City: {{address1.city}}</p>\n\n      <p>Pincode: {{address1.pincode}}</p>\n\n      <p>State: {{address1.state}}</p>\n\n    </ion-card-content>\n\n  </ion-card> -->\n\n\n\n    <ion-row>\n\n        <ion-col>\n\n          <button ion-button color="theme_dark" (click)="toggleFlag()">Add New Address</button>\n\n        </ion-col>\n\n    </ion-row>\n\n\n\n    <div *ngIf="addFlag">\n\n      <p text-center>(Note: All Fields Are Required)</p>\n\n    <div padding>\n\n      <form #shippingForm="ngForm" (ngSubmit)="toNext(address)">\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-list>\n\n                <ion-item>\n\n                    <div item-start class="icon-thumb"><ion-icon name="ios-home-outline"></ion-icon></div>\n\n                    <ion-input type="text" name="street" placeholder="Enter your first name" id="first_nameField" required [(ngModel)]="address.first_name"></ion-input>\n\n                  </ion-item>\n\n                <ion-item>\n\n                  <div item-start class="icon-thumb"><ion-icon name="ios-podium-outline"></ion-icon></div>\n\n                  <ion-input type="text" name="city" placeholder="Enter your last name" id="last_nameField" required [(ngModel)]="address.last_name"></ion-input>\n\n                </ion-item>\n\n              <ion-item>\n\n                <div item-start class="icon-thumb"><ion-icon name="ios-home-outline"></ion-icon></div>\n\n                <ion-input type="text" name="street" placeholder="Enter your first address" id="address_1Field" required [(ngModel)]="address.address_1"></ion-input>\n\n              </ion-item>\n\n              <ion-item>\n\n                  <div item-start class="icon-thumb"><ion-icon name="ios-home-outline"></ion-icon></div>\n\n                  <ion-input type="text" name="street" placeholder="Enter your second address" id="address_2Field" required [(ngModel)]="address.address_2"></ion-input>\n\n                </ion-item>\n\n              <ion-item>\n\n                <div item-start class="icon-thumb"><ion-icon name="ios-podium-outline"></ion-icon></div>\n\n                <ion-input type="text" name="city" placeholder="Enter City Name" id="cityField" required [(ngModel)]="address.city"></ion-input>\n\n              </ion-item>\n\n              <ion-item>\n\n                  <div item-start class="icon-thumb"><ion-icon name="ios-train-outline"></ion-icon></div>\n\n                  <ion-input type="text" name="state" placeholder="Enter State Name" id="stateField" required [(ngModel)]="address.state"></ion-input>\n\n                </ion-item>\n\n              <ion-item>\n\n                <div item-start class="icon-thumb"><ion-icon name="ios-pin-outline"></ion-icon></div>\n\n                <ion-input type="text" name="pincode" placeholder="Enter Pincode" id="pincodeField" required [(ngModel)]="address.pincode"></ion-input>\n\n              </ion-item>\n\n              <ion-item>\n\n                  <div item-start class="icon-thumb"><ion-icon name="ios-home-outline"></ion-icon></div>\n\n                  <ion-input type="text" name="locality" placeholder="Enter country" id="countryField" required [(ngModel)]="address.country"></ion-input>\n\n                </ion-item>\n\n  \n\n            </ion-list>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n          <button ion-button color="theme_dark" class="submit-button" type="submit" [disabled]="!shippingForm.form.valid">Next</button>\n\n        </ion-col>\n\n        </ion-row>\n\n      </form>  \n\n    </div>\n\n    </div>\n\n\n\n    <ion-row *ngIf="!addFlag">\n\n      <ion-col>\n\n          <button class="pymBtn" color="theme_dark" ion-button (click)="toNext(address1)" full>Payment</button>\n\n      </ion-col>\n\n    </ion-row>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\shipping\shipping.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], ShippingPage);
    return ShippingPage;
}());

//# sourceMappingURL=shipping.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_orders_orders__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__codmsg_codmsg__ = __webpack_require__(225);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * 5% GST if the taxable value of the goods does not exceed Rs.1000 per piece.
 * All types of apparel and clothing of sale value exceeding Rs. 1000 per piece
 *  would be taxed at 12% GST
 */
var PaymentPage = /** @class */ (function () {
    function PaymentPage(navCtrl, navParams, loadingCtrl, orderService, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.orderService = orderService;
        this.storage = storage;
        this.recievedData = {};
        this.total = 0;
        this.gst = 0;
        this.finalPrice = 0;
        this.deliveryCharges = 0;
        this.cartorder = [];
        this.recievedData = navParams.get('data');
        console.log("RECIEVED DATA: ", this.recievedData);
        this.total = this.recievedData.order_details.total;
        this.getFinalPrice(this.total);
        this.storage.get('cartorder').then(function (val) {
            _this.cartorder = val;
        });
        this.storage.get('userdetails').then(function (val) {
            _this.customer_id = val.id;
        });
    }
    PaymentPage.prototype.toNext = function () {
        // var options = {
        //   description: 'Credits towards consultation',
        //   image: 'https://i.imgur.com/3g7nmJC.png',
        //   currency: 'INR',
        //   key: 'rzp_test_1DP5mmOlF5G5ag',
        //   order_id: 'order_7HtFNLS98dSj8x',
        //   amount: '5000',
        //   name: 'foo',
        //   prefill: {
        //     email: 'pranav@razorpay.com',
        //     contact: '8879524924',
        //     name: 'Pranav Gupta'
        //   },
        //   theme: {
        //     color: '#F37254'
        //   }
        // }
        // var successCallback = function(success) {
        //   alert('payment_id: ' + success.razorpay_payment_id)
        //   var orderId = success.razorpay_order_id
        //   var signature = success.razorpay_signature
        // }
        // var cancelCallback = function(error) {
        //   alert(error.description + ' (Error '+error.code+')')
        // }
        // RazorpayCheckout.open(options, successCallback, cancelCallback)
        this.order();
        //
        // setTimeout(() => {
        // }, 10000);
        // this.navCtrl.push(HomePage);
        // this.order();
    };
    PaymentPage.prototype.order = function () {
        var _this = this;
        var order = {
            "order": {
                "payment_details": {
                    "method_id": "bacs",
                    "method_title": "Direct Bank Transfer",
                    "paid": true
                },
                "billing_address": {
                    "first_name": "John",
                    "last_name": "Doe",
                    "address_1": "969 Market",
                    "address_2": "",
                    "city": "San Francisco",
                    "state": "CA",
                    "postcode": "94103",
                    "country": "US",
                    "email": "john.doe@example.com",
                    "phone": "(555) 555-5555"
                },
                "shipping_address": this.recievedData.shipping_details.address,
                "customer_id": this.customer_id,
                "line_items": this.cartorder,
                "shipping_lines": [
                    {
                        "method_id": "flat_rate",
                        "method_title": "Flat Rate",
                        "total": 10
                    }
                ]
            }
        };
        var dataObj = {
            price: this.finalPrice,
            id: "OD123456789",
        };
        this.orderService.createOrder(order)
            .subscribe(function (res) {
            debugger;
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__codmsg_codmsg__["a" /* CodmsgPage */]);
        });
    };
    PaymentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentPage');
    };
    PaymentPage.prototype.getFinalPrice = function (total) {
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        if (total <= 1000) {
            this.gst = 0.05 * total;
        }
        else {
            this.gst = 0.12 * total;
        }
        // free delivery for products above 500 ruppees
        if (total > 500) {
            this.finalPrice = this.gst + total;
        }
        else {
            this.deliveryCharges = 100;
            this.finalPrice = this.gst + this.deliveryCharges + this.recievedData.order_details.total;
        }
        // setting delivery date i.e. after 5 days
        this.delDate = new Date();
        this.delDate.setDate(this.delDate.getDate() + 5);
        console.log("Delivery Date: ", this.delDate);
        this.hideLoading();
    };
    PaymentPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"D:\Ionic app\src\pages\payment\payment.html"*/'<!--\n\n  Generated template for the PaymentPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Payment</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content>\n\n  \n\n    <ion-card>\n\n      <ion-card-header>Payment Details</ion-card-header>\n\n  \n\n      <ion-row>\n\n        <ion-col>Base Total:</ion-col>\n\n        <ion-col>₹{{this.recievedData.order_details.total}}</ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>G.S.T.: </ion-col>\n\n        <ion-col>₹{{gst}}</ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>Delivery Charges: </ion-col>\n\n        <ion-col>₹{{deliveryCharges}}</ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>Billing Amount: </ion-col>\n\n        <ion-col>₹{{finalPrice}}</ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n  \n\n    <!-- <ion-card>\n\n      <ion-card-header>Delivery Date</ion-card-header>\n\n      <h2 text-center>{{delDate.toLocaleDateString(\'en-GB\')}} By 5:00 P.M.</h2>\n\n    </ion-card> -->\n\n  \n\n    <ion-card>\n\n      <ion-list radio-group [(ngModel)]="payment_method">\n\n        <ion-list-header>Choose Payment Method</ion-list-header>\n\n  \n\n        <ion-item>\n\n          <ion-label>Cash On Delivery</ion-label>\n\n          <ion-radio checked="true" value="cod"></ion-radio>\n\n        </ion-item>\n\n  \n\n        <!-- <ion-item>\n\n          <ion-label>Net Banking</ion-label>\n\n          <ion-radio value="netbanking"></ion-radio>\n\n        </ion-item>\n\n  \n\n        <ion-item>\n\n          <ion-label>Credit / Debit Card</ion-label>\n\n          <ion-radio value="card"></ion-radio>\n\n        </ion-item> -->\n\n      </ion-list>\n\n    </ion-card>\n\n  </ion-content>\n\n  \n\n  <ion-footer>\n\n    <button class="btmBtn" (click)="toNext()" ion-button full>Next</button>  \n\n  </ion-footer>\n\n  '/*ion-inline-end:"D:\Ionic app\src\pages\payment\payment.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__providers_orders_orders__["a" /* OrderProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_orders_orders__["a" /* OrderProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]) === "function" && _e || Object])
    ], PaymentPage);
    return PaymentPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CheckoutPage = /** @class */ (function () {
    function CheckoutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataRecieved = {};
        this.amount = 0;
        this.dataRecieved = navParams.get('data');
        this.amount = this.dataRecieved.price;
        this.items = this.dataRecieved.items;
        this.id = this.dataRecieved.id;
        console.log("DATA RECIEVED: ", this.dataRecieved);
        console.log("AMOUNT: " + this.amount);
    }
    CheckoutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CheckoutPage');
    };
    CheckoutPage.prototype.toHome = function () {
        this.navCtrl.popToRoot();
    };
    CheckoutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-checkout',template:/*ion-inline-start:"D:\Ionic app\src\pages\checkout\checkout.html"*/'<!--\n\n  Generated template for the CheckoutPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Checkout</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content id="pageBg">\n\n  \n\n    <div id="status-icon" text-center>\n\n      <ion-icon name="ios-checkmark-circle-outline"></ion-icon>\n\n    </div>\n\n  \n\n    <h1 style="font-size: 8vw;" text-center>Success</h1>\n\n    <h2 text-center text-wrap>Payment done successfully</h2>\n\n    <h3 text-center text-wrap>Your Order ID is:  {{id}}</h3>\n\n  \n\n    <ion-list>\n\n      <div class="item-div" *ngFor="let item of items" text-center text-wrap>\n\n        <p>{{item.name}}</p>\n\n        <p>Size: {{item.sizes[0]}}</p>\n\n        <p>Quantity: {{item.quantity}}</p>\n\n        <p>Price (per item): ₹{{item.price}}</p>\n\n      </div>\n\n    </ion-list>\n\n    <ion-list>\n\n      <div class="item-div" text-center text-wrap>\n\n        <h2>Total: ₹{{amount}}</h2>\n\n      </div>\n\n    </ion-list>\n\n    <button class="done-btn" ion-button text-center color="light" outline (click)="toHome()">Done</button>\n\n    \n\n  </ion-content>\n\n  '/*ion-inline-end:"D:\Ionic app\src\pages\checkout\checkout.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], CheckoutPage);
    return CheckoutPage;
}());

//# sourceMappingURL=checkout.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = /** @class */ (function () {
    function ProfilePage(storage, navCtrl, navParams) {
        var _this = this;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
        this.user_edit = {};
        this.address1 = {};
        this.address2 = {};
        debugger;
        storage.get('userdetails').then(function (val) {
            console.log('Your user is', val);
            _this.user = {
                firstname: val.name,
                email: val.email,
                mobile: 7011721626
            };
        });
        this.address1 = {
            street: "H-1, ABC",
            locality: "XYZ, EFG",
            city: "NCR",
            pincode: "200000",
            state: "UP"
        };
        this.address2 = {
            street: "H-2, XYZ",
            locality: "ABC, LMN",
            city: "DEL",
            pincode: "200200",
            state: "DEL"
        };
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\Ionic app\src\pages\profile\profile.html"*/'<!--\n\n  Generated template for the DashboardPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header class="profileNav">\n\n\n\n  <ion-navbar>\n\n    <ion-buttons left>\n\n      <button ion-button icon-only menuToggle>\n\n        <ion-icon name="ios-menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title></ion-title>\n\n\n\n    <ion-buttons right>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="ios-settings"></ion-icon>\n\n      </button>           \n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content id="profileBack">\n\n\n\n  <ion-content id="profileBanner" text-center padding>\n\n      <ion-thumbnail >\n\n        <img id="profileImg" src="assets/imgs/avatar-default.png" />\n\n      </ion-thumbnail>\n\n      <ion-title style="color: white !important;">{{user.firstname}} {{user.lastname}}</ion-title>     \n\n      <p>{{user.email}}</p> \n\n      <button ion-button small icon-only color="light" float-right><ion-icon name="md-create" right></ion-icon></button>\n\n  </ion-content>\n\n  \n\n  <div>\n\n    <ion-card>\n\n      <ion-card-header>{{user.firstname}} {{user.lastname}}</ion-card-header>\n\n      <ion-card-content>\n\n        <p>{{user.mobile}}</p>\n\n        <p>{{user.email}}</p>\n\n        \n\n      </ion-card-content>\n\n    </ion-card>\n\n  </div>\n\n  \n\n  <div>\n\n      <!-- <ion-card>\n\n        <ion-card-header>Change Password</ion-card-header>\n\n        <ion-card-content>\n\n          <ion-list>\n\n            <ion-item>\n\n              <div item-start class="icon-thumb"><ion-icon name="ios-key-outline"></ion-icon></div>\n\n                <ion-input type="text" name="oldpassword" placeholder="Enter Old Password" id="oldpasswordField" required [(ngModel)]="user_edit.oldpassword"></ion-input>\n\n              </ion-item>\n\n\n\n              <ion-item>\n\n                <div item-start class="icon-thumb"><ion-icon name="ios-key-outline"></ion-icon></div>\n\n                <ion-input type="text" name="newpassword" placeholder="Enter New Password" id="newpasswordField" required [(ngModel)]="user_edit.newpassword"></ion-input>\n\n              </ion-item>\n\n          </ion-list>\n\n        </ion-card-content>\n\n      </ion-card> -->\n\n\n\n      <ion-card>\n\n    <ion-card-header>Address 1</ion-card-header>\n\n    <ion-card-content>\n\n      <p>Street: {{address1.street}}</p>\n\n      <p>Locality: {{address1.locality}}</p>\n\n      <p>City: {{address1.city}}</p>\n\n      <p>Pincode: {{address1.pincode}}</p>\n\n      <p>State: {{address1.state}}</p>\n\n    </ion-card-content>\n\n  </ion-card>\n\n  </div>\n\n\n\n  <div style="margin-top: 10vh;">\n\n    <ion-list>\n\n      <ion-item>SETTINGS</ion-item>\n\n      <ion-item>LOGOUT</ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ForgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgetPage = /** @class */ (function () {
    function ForgetPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = {};
        this.emailFlag = false;
    }
    ForgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgetPage');
    };
    ForgetPage.prototype.submitEmail = function () {
        this.emailFlag = true;
    };
    ForgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forget',template:/*ion-inline-start:"D:\Ionic app\src\pages\forget\forget.html"*/'<!--\n\n  Generated template for the ForgetPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Forget</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content id="page_bg">\n\n\n\n  <div id="title_bg">\n\n  <div text-center style="font-size: 25vw; margin: 5vw; margin-bottom: 0vw; padding-top: 5vw;">\n\n    <img src="assets/imgs/theme_logo.png" />  \n\n  </div>\n\n\n\n  </div>\n\n\n\n\n\n  <div *ngIf="!emailFlag" padding>\n\n    <form #loginForm="ngForm" (ngSubmit)="submitEmail()">\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-list>\n\n            <ion-item>\n\n                <div item-start style="font-size: 5vw;"><ion-icon name="ios-mail-outline"></ion-icon></div>\n\n                <ion-input type="text" name="email" placeholder="Email" id="emailField" required [(ngModel)]="user.email" ></ion-input>\n\n            </ion-item>\n\n          </ion-list>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row>\n\n        <ion-col>\n\n          <button ion-button class="login-button" type="submit" [disabled]="!loginForm.form.valid">Submit</button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </form>\n\n  </div>\n\n\n\n  <div *ngIf="emailFlag">\n\n    <ion-card>\n\n      <p>Check your email address for link to change password.</p>\n\n    </ion-card>\n\n  </div>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\forget\forget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ForgetPage);
    return ForgetPage;
}());

//# sourceMappingURL=forget.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"D:\Ionic app\src\pages\about\about.html"*/'<!--\n\n  Generated template for the AboutPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>About</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n  <ion-card>\n\n    <ion-card-header class="head-card" text-center>About us</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        Mangli Hoisery is an online retail undergarment, swimwear and Lingerie store offering affordable clothing to every individual in India, fashion style for less to impress - its online clothing shopping not to be missed. \n\n\n\n        Time saving, reliability and shopping convenience are important to us; we offer convenient shopping for all occasions and accessories to match.\n\n    </p>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\about\about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_menu_menu__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sub_sub__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CategoryPage = /** @class */ (function () {
    function CategoryPage(navCtrl, navParams, dataService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.loadingCtrl = loadingCtrl;
        this.cat = navParams.get('data');
        this.getCategoryData(this.cat);
    }
    CategoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoryPage');
    };
    CategoryPage.prototype.getCategoryData = function (cat) {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        this.dataService.getCategory()
            .subscribe(function (data) {
            _this.data = data;
            console.log("ALL DATA: ", _this.data);
            _this.filtered = _this.data.filter(function (c) {
                return (c.category.toLowerCase() == _this.cat.toLowerCase());
            });
            _this.items = _this.filtered[0].subs;
            console.log("FILTERED: ", _this.filtered);
            console.log("ITEMS SUBS: ", Array.isArray(_this.items));
            _this.hideLoading();
        }, function (error) {
            console.log("SubPage Error: ", error);
            _this.hideLoading();
        });
    };
    CategoryPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    CategoryPage.prototype.openSub = function (subcat) {
        var selectedItem = {
            'category': this.cat,
            'subcategory': subcat
        };
        console.log("SELECTED: ", selectedItem);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__sub_sub__["a" /* SubPage */], {
            data: selectedItem
        });
    };
    CategoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-category',template:/*ion-inline-start:"D:\Ionic app\src\pages\category\category.html"*/'<!--\n\n  Generated template for the CategoryPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title text-capitalize> {{cat}}\'s Category</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-row>\n\n    <div class="box" *ngFor="let item of items" (click)="openSub(item.title)" text-uppercase>\n\n      <div>{{item.title}}</div>\n\n      <img class="thumb-icon" [src]="item.img" />\n\n    </div>\n\n  </ion-row>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\category\category.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_menu_menu__["a" /* MenuProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], CategoryPage);
    return CategoryPage;
}());

//# sourceMappingURL=category.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__images_images__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cart_cart__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_cart_cart__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetailsPage = /** @class */ (function () {
    function DetailsPage(navCtrl, navParams, modalCtrl, cartService, storage, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.cartService = cartService;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.product = {};
        this.showprice = false;
        this.price1 = '';
        this.price2 = '';
        this.variationid = '';
        this.size = [];
        this.cart = [];
        this.placingcart = [];
        this.color = [];
        this.show = false;
        storage.get('item').then(function (val) {
            _this.cart = val;
        });
        this.product = navParams.get('data');
        if (this.product.short_description !== "") {
            this.show = true;
            this.plain_text = this.htmlToPlaintext(this.product.short_description);
            this.product.description = this.htmlToPlaintext(this.product.description);
        }
        console.log("Product: ", this.product);
    }
    DetailsPage.prototype.htmlToPlaintext = function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
    DetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailsPage');
    };
    DetailsPage.prototype.PriceUpdater = function (product) {
        var last = product.attributes.length - 1;
        if (product.attributes[last].selection) {
            var self = this;
            this.a = product.variations.filter(function (e) {
                var i = 0;
                if (e.attributes[i].option == "") {
                    self.price1 = e.regular_price;
                    self.price2 = e.sale_price;
                    if (self.price1 == self.price2) {
                    }
                    else {
                        self.discount = ((parseInt(self.price1) - parseInt(self.price2)) / parseInt(self.price1)) * 100;
                        self.showprice = true;
                    }
                    return false;
                }
                while (i <= last) {
                    if ((e.attributes[i].option).toLowerCase() == (product.attributes[i].selection).toLowerCase()) {
                        if (i == last) {
                            self.price1 = e.regular_price;
                            self.price2 = e.sale_price;
                            self.variationid = e.id;
                            if (self.price1 == self.price2) {
                            }
                            else {
                                self.discount = ((parseInt(self.price1) - parseInt(self.price2)) / parseInt(self.price1)) * 100;
                                self.showprice = true;
                            }
                            return true;
                        }
                        i++;
                    }
                    else {
                        break;
                    }
                }
            });
        }
        console.log(this.a);
    };
    //presenting modal page
    DetailsPage.prototype.toImages = function (imgs) {
        this.imgModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__images_images__["a" /* ImagesPage */], { data: imgs });
        this.imgModal.onDidDismiss(function (data) {
            console.log(data);
        });
        this.imgModal.present();
    };
    DetailsPage.prototype.toCart = function () { this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */]); };
    DetailsPage.prototype.AddtoCart = function () {
        var _this = this;
        if (this.Quantity == undefined) {
            this.showalert("Please Select Quantity");
        }
        else {
            debugger;
            var carts = { total: this.product.price,
                name: this.product.title,
                thumnail: this.product.featured_src,
                product_id: this.product.id,
                quantity: this.Quantity,
                variation_id: this.variationid
            };
            var placingorder = {
                product_id: this.product.id,
                quantity: this.Quantity,
                variation_id: this.variationid
            };
            this.storage.get('userdetails').then(function (val) {
                console.log('Your user is', val);
                if (val !== "" && val !== null) {
                    // this.cartService.addProduct(this.product.id,this.Quantity).subscribe((res) => {
                    //   cart =res;
                    // });
                    _this.cart.push(carts);
                    _this.placingcart.push(placingorder);
                    _this.storage.set('cartorder', _this.placingcart);
                    _this.storage.set('item', _this.cart);
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__cart_cart__["a" /* CartPage */], {
                        data: carts
                    });
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
                }
            });
        }
    };
    DetailsPage.prototype.showalert = function (title) {
        var alert = this.alertCtrl.create({
            title: title,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"D:\Ionic app\src\pages\details\details.html"*/'<!--\n\n  Generated template for the DetailsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header class="trans-nav">\n\n\n\n    <ion-navbar>\n\n      <ion-title>Details</ion-title>\n\n      <ion-buttons right>\n\n        <button ion-button icon-only>\n\n          <ion-icon name="md-search"></ion-icon>\n\n        </button>\n\n  \n\n        <button ion-button icon-only>\n\n          <ion-icon name="ios-bookmark-outline"></ion-icon>\n\n        </button>\n\n  \n\n        <button ion-button (click)="toCart()" icon-only>\n\n          <ion-icon name="ios-basket"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content>\n\n    <ion-row>\n\n    <ion-slides class="detailThumb" pager>\n\n      <ion-slide *ngFor="let image of product.images" (click)="toImages(product.images.src)">\n\n        <img class="imgThumb" [src]=\'image.src\' />\n\n      </ion-slide>\n\n    </ion-slides>\n\n    </ion-row>\n\n  \n\n    <ion-row class="pageBack">  \n\n    <ion-card>\n\n      <ion-card-content>\n\n        <ion-row>\n\n          <ion-col>{{product.title}}</ion-col> <span class="rightText">₹{{product.price}}</span>\n\n        \n\n        </ion-row>\n\n        <ion-row *ngIf="showprice">\n\n          <ion-col>\n\n          <span > Regular price ₹{{price1}}</span>\n\n        </ion-col>\n\n        </ion-row>\n\n        <ion-row *ngIf="showprice">\n\n            <ion-col>\n\n                <span >Discount-{{discount}}%</span>\n\n          </ion-col>\n\n          </ion-row>\n\n        <ion-row *ngIf="showprice">\n\n            <ion-col>\n\n                <span >Sale Price ₹{{price2}}</span>\n\n          </ion-col>\n\n          </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>{{product.brand}}</ion-col> <span class="rightText">{{product.id}}</span>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  \n\n\n\n    <ion-card *ngFor="let array of product.attributes">\n\n      <ion-card-header>{{array.name}}</ion-card-header>\n\n      <ion-card-content>\n\n        <!-- <ion-row  >\n\n          <button  class="size-circle" \n\n          *ngFor="let sizearr of at"\n\n          ion-button (click)="selection(sizearr,1)">{{sizearr}}</button>\n\n        </ion-row> -->\n\n        <ion-item>\n\n          <ion-label>{{array.name}}</ion-label>\n\n          <ion-select [(ngModel)]="array.selection" (ionChange)="PriceUpdater(product)">\n\n              <ion-option [value]="sizearr" *ngFor="let sizearr of array.options">{{sizearr}}</ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n      </ion-card-content>\n\n    </ion-card>\n\n    \n\n  \n\n    <ion-card>\n\n      <ion-card-header>Description</ion-card-header>\n\n      <ion-card-content>{{product.description}}</ion-card-content>\n\n    </ion-card>\n\n    <ion-card>\n\n        <ion-item>\n\n          <ion-label>Quantity</ion-label>\n\n          <ion-select [(ngModel)]="Quantity">\n\n              <ion-option value="1">1</ion-option>\n\n              <ion-option value="2">2</ion-option>\n\n              <ion-option value="3">3</ion-option>\n\n              <ion-option value="4">4</ion-option>\n\n              <ion-option value="5">5</ion-option>\n\n              <ion-option value="6">6</ion-option>\n\n              <ion-option value="7">7</ion-option>\n\n              <ion-option value="8">8</ion-option>\n\n          </ion-select>\n\n        </ion-item>\n\n    </ion-card>\n\n    <ion-card *ngIf="this.product && this.show">\n\n      <ion-card-content>Material: {{plain_text}}</ion-card-content>\n\n    </ion-card>\n\n  \n\n    <ion-card *ngIf="this.product && this.show">\n\n      <ion-card-content>Care: {{plain_text}}</ion-card-content>\n\n    </ion-card>\n\n  \n\n    <ion-card>\n\n      <ion-card-content >Category: {{product.categories[0]}} </ion-card-content>\n\n    </ion-card>\n\n  \n\n    </ion-row>\n\n  </ion-content>\n\n  \n\n  <ion-footer>\n\n    <button class="btmBtn" color="theme_dark" ion-button (click)="AddtoCart()" full>Add to Cart</button>  \n\n  </ion-footer>\n\n  \n\n  '/*ion-inline-end:"D:\Ionic app\src\pages\details\details.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__providers_cart_cart__["a" /* CartProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_cart_cart__["a" /* CartProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _f || Object])
    ], DetailsPage);
    return DetailsPage;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ImagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ImagesPage = /** @class */ (function () {
    function ImagesPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.images = navParams.get('data');
        console.log("Images: ", this.images);
    }
    ImagesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ImagesPage');
    };
    ImagesPage.prototype.dismiss = function () {
        var returnMsg = 'Closed Images';
        this.viewCtrl.dismiss(returnMsg);
    };
    ImagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-images',template:/*ion-inline-start:"D:\Ionic app\src\pages\images\images.html"*/'<!--\n\n  Generated template for the ImagesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-buttons end>\n\n      <button ion-button icon-only (click)="dismiss()">\n\n        <ion-icon name="close"></ion-icon>\n\n      </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content>\n\n  \n\n    <ion-slides class="detailThumb" pager>\n\n      <ion-slide *ngFor="let image of images">\n\n        <img class="imgThumb" [src]=\'image\' />\n\n      </ion-slide>\n\n    </ion-slides>\n\n  \n\n  </ion-content>'/*ion-inline-end:"D:\Ionic app\src\pages\images\images.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */]])
    ], ImagesPage);
    return ImagesPage;
}());

//# sourceMappingURL=images.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Order_Details; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Order_Details = /** @class */ (function () {
    function Order_Details(navCtrl, navParams, cartService, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cartService = cartService;
        this.loadingCtrl = loadingCtrl;
        this.receivedData = {};
        this.receivedData = navParams.get('data');
    }
    Order_Details.prototype.alert = function (view) {
        if (view == 1) {
            alert("Return Accepted");
        }
        else {
            alert("Cancel is accepted");
        }
    };
    Order_Details.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrdersPage');
    };
    Order_Details.prototype.getData = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        this.cartService.getCart()
            .subscribe(function (res) {
            _this.items = res;
            console.log("CART", _this.items);
        });
        this.hideLoading();
    };
    Order_Details.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    Order_Details = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'order-details',template:/*ion-inline-start:"D:\Ionic app\src\pages\order_details\order_details.html"*/'<!--\n\n  Generated template for the OrdersPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Order Details</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n<ion-row >\n\n      <ion-card>\n\n        <ion-row>\n\n          <ion-col col-9>\n\n            <ion-row><h2 text-wrap>{{receivedData.name}}</h2></ion-row></ion-col> \n\n            \n\n         \n\n          \n\n          <ion-col col-3>\n\n            <img class="thumbnail" [src]="receivedData.thumbnail" />\n\n\n\n          </ion-col>\n\n          <ion-col col-12>\n\n            <ion-row>{{receivedData.brand}}</ion-row>\n\n            <ion-row>Size: {{receivedData.Size}}</ion-row>\n\n            <ion-row>Quantity: {{receivedData.total_line_items_quantity}}</ion-row>\n\n            <ion-row>Shipping Details: ₹{{receivedData.total_shipping}}</ion-row>\n\n            <ion-row>Tax: ₹{{receivedData.total_tax}}</ion-row>\n\n            <ion-row>Total: ₹{{receivedData.total}}</ion-row>\n\n            <ion-row>\n\n              <ion-col col-6>\n\n               <button ion-button class="login-button" (click)="alert(1)">Return</button>\n\n              </ion-col>\n\n              <ion-col col-6>\n\n               <button ion-button class="login-button"  (click)="alert(2)" >Cancel</button>\n\n              </ion-col>\n\n            </ion-row>\n\n            </ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n</ion-row>        \n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\order_details\order_details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], Order_Details);
    return Order_Details;
}());

//# sourceMappingURL=order_details.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PrivacyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrivacyPage = /** @class */ (function () {
    function PrivacyPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PrivacyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PrivacyPage');
    };
    PrivacyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-privacy',template:/*ion-inline-start:"D:\Ionic app\src\pages\privacy\privacy.html"*/'<!--\n\n  Generated template for the PrivacyPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Privacy</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card>\n\n    <ion-card-header id="head-card" text-center>Privacy notice</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        Thank you for visiting our web site. This privacy policy tells you how we use personal information collected at this site. Please read this privacy policy before using the site or submitting any personal information. By using the site, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the site to make sure that you understand how any personal information you provide will be used.\n\n        Note, the privacy practices set forth in this privacy policy are for this web site only. If you link to other web sites, please review the privacy policies posted at those sites.\n\n        We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfill your specific request. This information is only used to fulfill your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.\n\n        We possess the right of using it’s user’s information just to keep them acknowledged of the products and services that are or will be provided via a communication channel, unless or until you ask us not to do.\n\n        Cookie/Tracking Technology The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology, however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.\n\n        In accordance with some of Google’s recent compulsory policies, we herewith inform you that we might be tracking your cookies.\n\n        The Company shall under no circumstances be using any sensitive information of any nature on any of our re marketing tags.\n\n    </p>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header>Privacy Contact Information</ion-card-header>\n\n    <ion-card-content>\n\n        <p text-wrap>If you have any questions, concerns, or comments about our privacy policy you may contact us using the information below:</p>\n\n\n\n        <p text-wrap>By E-Mail: info@undergarment.co.in ; service@undergarment.co.in</p>\n\n        <p text-wrap>We reserve the right to make changes to this policy. Any changes to this policy will be posted.</p>\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\privacy\privacy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], PrivacyPage);
    return PrivacyPage;
}());

//# sourceMappingURL=privacy.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReturnPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ReturnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReturnPage = /** @class */ (function () {
    function ReturnPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ReturnPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReturnPage');
    };
    ReturnPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-return',template:/*ion-inline-start:"D:\Ionic app\src\pages\return\return.html"*/'<!--\n\n  Generated template for the ReturnPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Return</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card >\n\n    <ion-card-header id="head-card" text-center>REFUND / RETURN POLICY</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        The return policy contrives the opulence of cancelling the order without any cancellation charges involved until the order is not out for delivery yet. You may check the status of your order in your undergarment.co.in account ORDERS section. The order will discontinue to undergo cancellation if its status is OUT FOR DELIVERY. \n\n\n\n        For returning the product a few viables are required:\n\n        \n\n        The reason for return should be wrong size received/wrong color received/damaged item received. Any reason apart for these won\'t be entertained.\n\n        You may initiate the returns by logging in your account and filling out the form for returns. We obligate a 2 working days time in order to investigate the return. If found an apt retrun reason, the return will be initiated and picked accordingly in a 2-4 days time.\n\n        Further, the above mentioned machines help us to carry out our entire production process in smooth and effective manner.\n\n        \n\n        Due to hygiene reasons, we are unable to take return for any product that has been worn even once.\n\n        \n\n        For more information about how to return your purchase please contact our Support Team on (021) 853-4430, email info@undergarment.co.in alternatively please read Mangli Hoisery Returns Policy.\n\n    </p>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header>Contact Details</ion-card-header>\n\n    <ion-card-content>\n\n        <p>Warehouse Physical address: 426, Turab Nagar, GZB-201001</p>\n\n\n\n        <p>Email address: info@undergarment.co.in</p>\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\return\return.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ReturnPage);
    return ReturnPage;
}());

//# sourceMappingURL=return.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_orders_orders__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_details_order_details__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrdersPage = /** @class */ (function () {
    function OrdersPage(navCtrl, navParams, orderService, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.orderService = orderService;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.getData();
    }
    OrdersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrdersPage');
    };
    OrdersPage.prototype.OrderDetails = function (item) {
        debugger;
        // this.orderService.ViewOrder(item.id).subscribe((res) => {
        //   debugger
        // });
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__order_details_order_details__["a" /* Order_Details */], {
            data: item
        });
    };
    OrdersPage.prototype.getData = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        debugger;
        this.storage.get('userdetails').then(function (val) {
            if (val !== '' && val !== null) {
                debugger;
                _this.orderService.getOrders(val.id)
                    .subscribe(function (res) {
                    _this.items = res.orders;
                    console.log("CART", _this.items);
                });
            }
            else {
                _this.navCtrl.pop().then(function () { _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]); });
            }
        });
        this.hideLoading();
    };
    OrdersPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    OrdersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-orders',template:/*ion-inline-start:"D:\Ionic app\src\pages\orders\orders.html"*/'<!--\n\n  Generated template for the OrdersPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Orders</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n<ion-row *ngFor="let item of items; let i = index">\n\n      <ion-card (click)="OrderDetails(item)">\n\n        <ion-row>\n\n          <ion-col col-9>\n\n            <ion-row><h2 text-wrap>{{item.order_number}}</h2></ion-row>\n\n            <ion-row>Total: ₹{{item.total}}</ion-row>\n\n            <ion-row>Order ID: {{item.id}}</ion-row>\n\n            <ion-row>Date: {{item.completed_at}}</ion-row>\n\n          </ion-col>  \n\n        </ion-row>\n\n      </ion-card>\n\n</ion-row>        \n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\orders\orders.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_orders_orders__["a" /* OrderProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_orders_orders__["a" /* OrderProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]) === "function" && _e || Object])
    ], OrdersPage);
    return OrdersPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=orders.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TermsPage = /** @class */ (function () {
    function TermsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TermsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TermsPage');
    };
    TermsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-terms',template:/*ion-inline-start:"D:\Ionic app\src\pages\terms\terms.html"*/'<!--\n\n  Generated template for the TermsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Terms & Conditions</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card>\n\n    <ion-card-header class="head-card" text-center>Representation of Goods</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        Every effort is made to represent goods accurately & precisely, the reproduction of colors is as accurate as photographic and publishing processes will allow. Every effort is made to describe goods accurately. All photography is for illustravie purposes only and quality of item cannot not depicted based on the photography.\n\n    </p>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header class="head-card" text-center>Use of Site</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        You may only use this site to browse the content, make legitimate purchases and shall not use this site for any other purposes, including without limitation, to make any speculative, false or fraudulent purchase. This site and the content provided in this site may not be copied, reproduced, republished, uploaded, posted, transmitted or distributed. \'Deep-linking\', \'embedding\' or using analogous technology is strictly prohibited. Unauthorized use of this site and/or the materials contained on this site may violate applicable copyright, trademark or other intellectual property laws or other laws.\n\n    </p>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header class="head-card" text-center>Disclaimer of Warranty</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        The contents of this site are provided "as is" without warranty of any kind, either expressed or implied, including but not limited to warranties of merchant-ability, fitness for a purpose and non-infringement.\n\n\n\n        The owner of this site, the authors of these contents and in general anybody connected to this site in any way, from now on collectively called "Providers", assume no responsibility for errors or omissions in these contents.\n\n        \n\n        The Providers further do not warrant, guarantee or make any representation regarding the safety, reliability, accuracy, correctness or completeness of these contents. The Providers shall not be liable for any direct, indirect, general, special, incidental or consequential damages (including -without limitation- data loss, lost revenues and lost profit) which may result from the inability to use or the correct or incorrect use, abuse, or misuse of these contents, even if the Providers have been informed of the possibilities of such damages. The Providers cannot assume any obligation or responsibility.\n\n        \n\n        The use of these contents is forbidden in those places where the law does not allow this disclaimer to take full effect.\n\n    </p>\n\n  </ion-card>\n\n\n\n  <ion-card>\n\n    <ion-card-header class="head-card" text-center>Our Rights</ion-card-header>\n\n  </ion-card>\n\n\n\n  <ion-card padding>\n\n    <p text-wrap>\n\n        We reserve the right to:\n\n\n\n        modify or withdraw, temporarily or permanently, the Website (or any part of) with or without notice to you and you confirm that we shall not be liable to you or any third party for any modification to or withdrawal of the Website; and/or\n\n        change these Conditions from time to time, and your continued use of the Website (or any part of) following such change shall be deemed to be your acceptance of such change. It is your responsibility to check regularly to determine whether the Conditions have been changed. If you do not agree to any change to the Conditions then you must immediately stop using the Website.\n\n        We will use our reasonable endeavours to maintain the Website. The Website is subject to change from time to time. You will not be eligible for any compensation because you cannot use any part of the Website or because of a failure, suspension or withdrawal of all or part of the Website due to circumstances beyond our control.\n\n    </p>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\terms\terms.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], TermsPage);
    return TermsPage;
}());

//# sourceMappingURL=terms.js.map

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 139;

/***/ }),

/***/ 180:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		304,
		20
	],
	"../pages/cart/cart.module": [
		303,
		19
	],
	"../pages/category/category.module": [
		305,
		18
	],
	"../pages/checkout/checkout.module": [
		306,
		17
	],
	"../pages/codmsg/codmsg.module": [
		313,
		16
	],
	"../pages/details/details.module": [
		307,
		15
	],
	"../pages/forget/forget.module": [
		308,
		14
	],
	"../pages/guest_checkout/guest_checkout.module": [
		309,
		13
	],
	"../pages/images/images.module": [
		310,
		12
	],
	"../pages/login/login.module": [
		311,
		11
	],
	"../pages/order_details/order_details.module": [
		312,
		10
	],
	"../pages/orders/orders.module": [
		317,
		9
	],
	"../pages/payment/payment.module": [
		318,
		8
	],
	"../pages/privacy/privacy.module": [
		314,
		7
	],
	"../pages/profile/profile.module": [
		315,
		6
	],
	"../pages/return/return.module": [
		316,
		5
	],
	"../pages/shipping/shipping.module": [
		319,
		4
	],
	"../pages/signup/signup.module": [
		320,
		3
	],
	"../pages/sub/sub.module": [
		322,
		2
	],
	"../pages/terms/terms.module": [
		321,
		1
	],
	"../pages/wishlist/wishlist.module": [
		323,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 180;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CodmsgPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CodmsgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CodmsgPage = /** @class */ (function () {
    function CodmsgPage(navCtrl, navParams, cartService, storage, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cartService = cartService;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.items = [];
        this.total = 0;
        this.show = false;
        this.DataObj = {};
        this.getCart();
    }
    CodmsgPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CartPage');
    };
    CodmsgPage.prototype.getCart = function () {
        var _this = this;
        // setting delivery date i.e. after 5 days
        this.delDate = new Date();
        this.delDate.setDate(this.delDate.getDate() + 5);
        // console.log("Delivery Date: ", this.delDate);
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        this.storage.get('item').then(function (val) {
            if (val.length == 0) {
                _this.show = true;
                _this.getTotal(_this.items);
            }
            else {
                _this.items = val;
                _this.getTotal(_this.items);
            }
        });
        this.hideLoading();
    };
    CodmsgPage.prototype.getTotal = function (items) {
        debugger;
        if (items.length !== 0) {
            for (var i = 0; i < items.length; i++) {
                this.total += parseFloat(items[i].total) * parseFloat(items[i].quantity);
                //  this.getFinalPrice(this.total);  
                if (this.total <= 1000) {
                    this.gst = 0.05 * this.total;
                }
                else {
                    this.gst = 0.12 * this.total;
                }
                // free delivery for products above 500 ruppees
                if (this.total > 500) {
                    this.finalPrice = this.gst + this.total;
                }
                else {
                    this.deliveryCharges = 100;
                    this.finalPrice = this.gst + this.deliveryCharges + this.total;
                }
                this.total = this.finalPrice;
            }
        }
        var getsum = function (total, currentItem) { return total + currentItem.price; };
        // this.cartService.carttotal().subscribe((res) => {
        //   this.total=res.total;
        // });
    };
    CodmsgPage.prototype.getFinalPrice = function (total) {
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        if (total <= 1000) {
            this.gst = 0.05 * total;
        }
        else {
            this.gst = 0.12 * total;
        }
        // free delivery for products above 500 ruppees
        if (total > 500) {
            this.finalPrice = this.gst + total;
        }
        else {
            this.deliveryCharges = 100;
            this.finalPrice = this.gst + this.deliveryCharges + this.recievedData.order_details.total;
        }
        // setting delivery date i.e. after 5 days
        this.delDate = new Date();
        this.delDate.setDate(this.delDate.getDate() + 5);
        console.log("Delivery Date: ", this.delDate);
        this.hideLoading();
    };
    CodmsgPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    CodmsgPage.prototype.ddte = function () {
    };
    CodmsgPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-codmsg',template:/*ion-inline-start:"D:\Ionic app\src\pages\codmsg\codmsg.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>THANKS</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card>\n    SUCCESSFUL ORDER. THANKYOU!\n\n    \n  </ion-card>\n    \n\n\n  <ion-row *ngFor="let item of items; let i = index">\n      <ion-card>\n        <ion-row>\n          <ion-col col-9>\n            <ion-row><h2 text-wrap>{{item.name}}</h2></ion-row>\n            <ion-row>{{item.brand}}</ion-row>\n            <ion-row>Price: ₹{{total}}</ion-row>\n            <ion-row><h2>Quantity: {{item.quantity}}</h2></ion-row>\n            <ion-row ><h2>Delivery Date: {{delDate}}</h2></ion-row>\n          </ion-col>\n          \n          <ion-col col-3>\n            <img class="img-thumb" [src]="item.thumnail" />\n          </ion-col>\n        </ion-row>\n      </ion-card>\n    </ion-row>\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\codmsg\codmsg.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]])
    ], CodmsgPage);
    return CodmsgPage;
}());

//# sourceMappingURL=codmsg.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(247);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_order_details_order_details__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_menu_menu__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__node_modules_angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_cart_cart__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_sub_sub__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_category_category__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_details_details__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_images_images__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_product_product__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_cart_cart__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_login_login__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_orders_orders__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_shipping_shipping__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_payment_payment__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_checkout_checkout__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_return_return__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_privacy_privacy__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_orders_orders__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_terms_terms__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_profile_profile__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_forget_forget__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_about_about__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_codmsg_codmsg__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_guest_checkout_guest_checkout__ = __webpack_require__(114);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_cart_cart__["a" /* CartPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_sub_sub__["a" /* SubPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_category_category__["a" /* CategoryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_details_details__["a" /* DetailsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_images_images__["a" /* ImagesPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_shipping_shipping__["a" /* ShippingPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_return_return__["a" /* ReturnPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_orders_orders__["a" /* OrdersPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_privacy_privacy__["a" /* PrivacyPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_terms_terms__["a" /* TermsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_forget_forget__["a" /* ForgetPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_guest_checkout_guest_checkout__["a" /* guest_checkout */],
                __WEBPACK_IMPORTED_MODULE_5__pages_order_details_order_details__["a" /* Order_Details */],
                __WEBPACK_IMPORTED_MODULE_32__pages_codmsg_codmsg__["a" /* CodmsgPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_10__node_modules_angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/cart/cart.module#CartPageModule', name: 'CartPage', segment: 'cart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/category/category.module#CategoryPageModule', name: 'CategoryPage', segment: 'category', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/checkout/checkout.module#CheckoutPageModule', name: 'CheckoutPage', segment: 'checkout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/details/details.module#DetailsPageModule', name: 'DetailsPage', segment: 'details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forget/forget.module#ForgetPageModule', name: 'ForgetPage', segment: 'forget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/guest_checkout/guest_checkout.module#guest_checkoutModule', name: 'guest_checkout', segment: 'guest_checkout', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/images/images.module#ImagesPageModule', name: 'ImagesPage', segment: 'images', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order_details/order_details.module#Order_DetailsModule', name: 'Order_Details', segment: 'order_details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/codmsg/codmsg.module#CodmsgPageModule', name: 'CodmsgPage', segment: 'codmsg', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/privacy/privacy.module#PrivacyPageModule', name: 'PrivacyPage', segment: 'privacy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/return/return.module#ReturnPageModule', name: 'ReturnPage', segment: 'return', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/orders/orders.module#OrdersPageModule', name: 'OrdersPage', segment: 'orders', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/shipping/shipping.module#ShippingPageModule', name: 'ShippingPage', segment: 'shipping', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/terms/terms.module#TermsPageModule', name: 'TermsPage', segment: 'terms', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sub/sub.module#SubPageModule', name: 'SubPage', segment: 'sub', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/wishlist/wishlist.module#WishlistPageModule', name: 'WishlistPage', segment: 'wishlist', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_cart_cart__["a" /* CartPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_sub_sub__["a" /* SubPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_category_category__["a" /* CategoryPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_details_details__["a" /* DetailsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_images_images__["a" /* ImagesPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_shipping_shipping__["a" /* ShippingPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_checkout_checkout__["a" /* CheckoutPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_return_return__["a" /* ReturnPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_orders_orders__["a" /* OrdersPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_privacy_privacy__["a" /* PrivacyPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_terms_terms__["a" /* TermsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_forget_forget__["a" /* ForgetPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_guest_checkout_guest_checkout__["a" /* guest_checkout */],
                __WEBPACK_IMPORTED_MODULE_5__pages_order_details_order_details__["a" /* Order_Details */],
                __WEBPACK_IMPORTED_MODULE_32__pages_codmsg_codmsg__["a" /* CodmsgPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_18__providers_product_product__["a" /* ProductProvider */],
                __WEBPACK_IMPORTED_MODULE_19__providers_cart_cart__["a" /* CartProvider */],
                __WEBPACK_IMPORTED_MODULE_20__providers_login_login__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_orders_orders__["a" /* OrderProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 302:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_sub_sub__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_signup_signup__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_return_return__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_orders_orders__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_terms_terms__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_privacy_privacy__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_about_about__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuService, loadingCtrl, storage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.menuService = menuService;
        this.storage = storage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.hide = false;
        this.initializeApp();
        storage.get('userdetails').then(function (val) {
            console.log('Your user is', val);
            if (val !== "" && val !== null) {
                _this.user = val;
                _this.hide = true;
            }
        });
        this.otherpages = [
            { title: 'About Us', component: __WEBPACK_IMPORTED_MODULE_13__pages_about_about__["a" /* AboutPage */], icon: 'ios-information-circle-outline' },
            { title: 'Orders', component: __WEBPACK_IMPORTED_MODULE_10__pages_orders_orders__["a" /* OrdersPage */], icon: 'ios-home-outline' },
            { title: 'T&C', component: __WEBPACK_IMPORTED_MODULE_11__pages_terms_terms__["a" /* TermsPage */], icon: 'ios-woman-outline' },
            { title: 'Return Policy', component: __WEBPACK_IMPORTED_MODULE_9__pages_return_return__["a" /* ReturnPage */], icon: 'ios-man-outline' },
            { title: 'Privacy Policy', component: __WEBPACK_IMPORTED_MODULE_12__pages_privacy_privacy__["a" /* PrivacyPage */], icon: 'ios-happy-outline' },
        ];
        console.log("OTHER PAGES: ", this.otherpages);
        this.getMenuData();
        this.activePage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.getMenuData = function () {
        var _this = this;
        this.menuService.getMenu()
            .subscribe(function (res) {
            _this.pages = res;
            console.log("MENU", _this.pages);
        });
    };
    MyApp.prototype.toggleSection = function (i) {
        // basic functionality i.e. toggling function
        this.pages[i].open = !this.pages[i].open;
    };
    MyApp.prototype.toggleSub = function (i, j, subs) {
        //this.pages[i].subs[j].open = !this.pages[i].subs[j].open; 
    };
    MyApp.prototype.openSub = function (i, j) {
        var selectedItem = {
            'category': this.pages[i].category,
            'subcategory': this.pages[i].subs[j].subcategory
        };
        console.log("SELECTED: ", selectedItem);
        this.nav.push(__WEBPACK_IMPORTED_MODULE_6__pages_sub_sub__["a" /* SubPage */], {
            data: selectedItem
        });
    };
    MyApp.prototype.toLogin = function (call) {
        if (call == 2) {
            this.storage.set('userdetails', "");
        }
        this.nav.push(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.toSignup = function () { this.nav.push(__WEBPACK_IMPORTED_MODULE_8__pages_signup_signup__["a" /* SignupPage */]); };
    MyApp.prototype.openPage = function (page) {
        this.nav.push(page.component);
        this.activePage = page;
    };
    MyApp.prototype.checkActive = function (page) {
        return page == this.activePage;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"D:\Ionic app\src\app\app.html"*/'<ion-menu [content]="content">\n\n    <ion-header>\n\n        <div>\n\n            <ion-row>\n\n                <ion-col col-3>\n\n                    <img id="menuImg" src="assets/imgs/avatar-default.png" />\n\n                </ion-col>\n\n                <ion-col>\n\n                    <ion-row  *ngIf="!hide" id="guest-msg">Welcome, Guest</ion-row>\n\n                    <ion-row  *ngIf="hide && this.user" id="guest-msg">Welcome, {{this.user.username}}</ion-row>\n\n                <ion-row no-padding *ngIf="!hide">\n\n                    <ion-col>\n\n                        <button class="smallBtn" ion-button (click)="toLogin(1)" small menuClose>Login</button>\n\n                    </ion-col>\n\n                    <ion-col>\n\n                        <button class="smallBtn" ion-button (click)="toSignup()" small menuClose>Signup</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n                <ion-row no-padding *ngIf="hide">\n\n                    <ion-col>\n\n                        <button class="smallBtn" ion-button (click)="toLogin(2)" small menuClose>Logout</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n    </ion-header>\n\n\n\n    <ion-content>\n\n        <ion-list class="menu-list" no-padding>\n\n            <ion-item *ngFor="let p of pages; let i = index" text-wrap no-lines no-padding>\n\n                <button ion-item (click)="toggleSection(i)" [ngClass]="{\'section-active\': p.open, \'section\': !p.open}" text-uppercase>\n\n                    <ion-icon item-right name="ios-arrow-forward-outline" *ngIf="!p.open"></ion-icon>\n\n                    <ion-icon item-right name="ios-arrow-down-outline" *ngIf="p.open"></ion-icon>\n\n                    <ion-icon [name]="p.icon"></ion-icon> &nbsp; {{p.category}}\n\n                </button>\n\n\n\n                <ion-list *ngIf="p.subs && p.open">\n\n                    <!--Second Level-->\n\n                    <ion-item *ngFor="let s of p.subs; let j = index" (click)="openSub(i, j)" [ngClass]="{\'sub-active\': s.open, \'sub\': !s.open}"  text-wrap text-uppercase menuToggle>\n\n                        {{s.subcategory}}\n\n                    </ion-item>\n\n                </ion-list>\n\n            </ion-item>\n\n\n\n            <ion-item text-wrap no-lines no-padding text-uppercase>\n\n                <button menuClose ion-item class="section" *ngFor="let op of otherpages" (click)="openPage(op)">\n\n                    <ion-icon item-right name="ios-arrow-forward-outline"></ion-icon>\n\n                    <ion-icon [name]="op.icon"></ion-icon> &nbsp; {{op.title}}\n\n                </button>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n        \n\n    </ion-content>\n\n</ion-menu>\n\n\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n'/*ion-inline-end:"D:\Ionic app\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__["a" /* MenuProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_14__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CartProvider = /** @class */ (function () {
    function CartProvider(http) {
        this.http = http;
        this.myheader = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]();
        this.myheader.append('Content-Type', 'application/json');
        console.log('Hello CartProvider Provider');
    }
    CartProvider.prototype.getCart = function () {
        return this.http.get('https://www.undergarment.co.in/wp-json/wc/v2/cart')
            .map(function (res) { return res; });
    };
    CartProvider.prototype.addProduct = function (prodid, quantity) {
        return this.http.post('https://www.undergarment.co.in/wp-json/wc/v2/cart/add', {
            "product_id": prodid,
            "variation": {
                "attribute_pa_color": "light",
                "attribute_pa_size": "m-medium"
            },
            "quantity": quantity
        }, { headers: this.myheader })
            .map(function (res) { return res; });
    };
    CartProvider.prototype.clearcart = function () {
        return this.http.get('https://www.undergarment.co.in/wp-json/wc/v2/cart/clear')
            .map(function (res) { return res; });
    };
    CartProvider.prototype.carttotal = function () {
        return this.http.get('https://www.undergarment.co.in/wp-json/wc/v2/cart/totals')
            .map(function (res) { return res; });
    };
    CartProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], CartProvider);
    return CartProvider;
}());

//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_profile__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forget_forget__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_login_login__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(storage, navCtrl, navParams, signupservice) {
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signupservice = signupservice;
        this.user = {};
        this.toggleBtn = false;
        this.typename = "password";
    }
    LoginPage.prototype.toggleEye = function () {
        console.log("PREV: " + this.toggleBtn);
        if (this.toggleBtn === true) {
            this.toggleBtn = false;
            this.typename = "password";
        }
        else {
            this.toggleBtn = true;
            this.typename = "text";
        }
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        console.log(this.user);
        this.signupservice.getlogin(2, this.user).subscribe(function (res) {
            debugger;
            if (res.status == "ok") {
                _this.userdetails = { username: res.user.username, email: res.user.email, name: res.user.displayname, id: res.user.id };
                _this.storage.set('userdetails', _this.userdetails);
                _this.navCtrl.pop().then(function () { _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__profile_profile__["a" /* ProfilePage */]); });
            }
            else {
                alert(res.error);
            }
        });
    };
    LoginPage.prototype.toForget = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__forget_forget__["a" /* ForgetPage */]);
    };
    LoginPage.prototype.toSignup = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"D:\Ionic app\src\pages\login\login.html"*/'<!--\n\n  Generated template for the LoginPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Login</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content id="page_bg">\n\n  <div id="title_bg">\n\n  <div text-center style="font-size: 25vw; margin: 5vw; margin-bottom: 0vw; padding-top: 5vw;">\n\n    <img src="assets/imgs/theme_logo.png" />  \n\n  </div>\n\n\n\n  </div>\n\n\n\n  <div padding>\n\n    <form #loginForm="ngForm" (ngSubmit)="login()">\n\n      <ion-row>\n\n        <ion-col>\n\n          <ion-list>\n\n            <ion-item>\n\n                <div item-start style="font-size: 5vw;"><ion-icon name="ios-mail-outline"></ion-icon></div>\n\n                <ion-input type="text" name="email" placeholder="Email" id="emailField" required [(ngModel)]="user.email" ></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n                <div item-start style="font-size: 5vw;"><ion-icon name="ios-key-outline"></ion-icon></div>\n\n                <ion-input [type]="typename" name="password" placeholder="Password" id="passwordField" required [(ngModel)]="user.password" ></ion-input>\n\n                <button *ngIf="!toggleBtn" ion-button clear item-end style="font-size: 5vw;" color="dark" (click)="toggleEye()" icon-only><ion-icon name="ios-eye-off-outline"></ion-icon></button>\n\n                <button *ngIf="toggleBtn" ion-button clear item-end style="font-size: 5vw;" (click)="toggleEye()" icon-only><ion-icon name="ios-eye-outline"></ion-icon></button>\n\n            </ion-item>\n\n          </ion-list>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-row>\n\n        <ion-col>\n\n          <button ion-button class="login-button" type="submit" [disabled]="!loginForm.form.valid">Login</button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </form>\n\n  </div>\n\n\n\n  <div>\n\n    <ion-row class="other-text">\n\n   <ion-col class="fp" (click)="toForget()">\n\n     Forget Password?\n\n   </ion-col>\n\n   <ion-col>Or</ion-col>\n\n   <ion-col class="sg" (click)="toSignup()">\n\n     Signup\n\n   </ion-col>\n\n </ion-row>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"D:\Ionic app\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__providers_login_login__["a" /* LoginProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_login_login__["a" /* LoginProvider */]) === "function" && _d || Object])
    ], LoginPage);
    return LoginPage;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__guest_checkout_guest_checkout__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CartPage = /** @class */ (function () {
    function CartPage(navCtrl, navParams, cartService, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cartService = cartService;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.items = [];
        this.total = 0;
        this.show = false;
        this.DataObj = {};
        this.getCart();
    }
    CartPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CartPage');
    };
    CartPage.prototype.getCart = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            spinner: 'ios',
            content: "Loading..."
        });
        this.loading.present();
        this.storage.get('item').then(function (val) {
            if (val.length == 0) {
                _this.show = true;
            }
            else {
                _this.items = val;
                _this.getTotal(_this.items);
            }
        });
        this.hideLoading();
    };
    CartPage.prototype.getTotal = function (items) {
        debugger;
        if (items.length !== 0) {
            for (var i = 0; i < items.length; i++) {
                this.total += parseFloat(items[i].total) * parseFloat(items[i].quantity);
            }
        }
        var getsum = function (total, currentItem) { return total + currentItem.price; };
        // this.cartService.carttotal().subscribe((res) => {
        //   this.total=res.total;
        // });
    };
    CartPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    CartPage.prototype.addItem = function (item) {
        //(item.quantity < 10) ? item.quantity=item.quantity+1 : 0
        if (item.quantity < 10) {
            item.quantity = item.quantity + 1;
            this.total = this.total + parseFloat(item.total);
        }
        this.storage.set('item', this.items);
    };
    CartPage.prototype.reduceItem = function (item) {
        //(item.quantity >= 1) ? item.quantity=item.quantity-1 : 0
        if (item.quantity >= 1) {
            item.quantity = item.quantity - 1;
            this.total = this.total - parseFloat(item.total);
        }
        this.storage.set('item', this.items);
    };
    CartPage.prototype.emptycart = function () {
        this.items = [];
        this.storage.set('item', this.items);
        this.show = true;
    };
    CartPage.prototype.deleteItem = function (idx) {
        //before deleting reduce price
        this.total = this.total - parseFloat(this.items[idx].total) * parseFloat(this.items[idx].quantity);
        this.items.splice(idx, 1);
        this.storage.set('item', this.items);
    };
    CartPage.prototype.toCheckout = function (items) {
        // also needs to check that items must not be empty
        if (this.items.length !== 0) {
            this.DataObj.items = items;
            this.DataObj.total = this.total;
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__guest_checkout_guest_checkout__["a" /* guest_checkout */], {
                data: this.DataObj
            });
        }
    };
    CartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cart',template:/*ion-inline-start:"D:\Ionic app\src\pages\cart\cart.html"*/'<!--\n\n  Generated template for the CartPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Cart</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content >\n\n  \n\n    <ion-card *ngIf="!show">\n\n      <ion-row>\n\n        <ion-col><ion-title>Total</ion-title></ion-col>\n\n        <ion-col><ion-title float-right>₹{{total}}</ion-title></ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n    <ion-card *ngIf="show">\n\n      <ion-row >\n\n        <ion-col><ion-title>Cart is Empty!</ion-title></ion-col>\n\n      </ion-row>\n\n    </ion-card>\n\n    <ion-row *ngFor="let item of items; let i = index" >\n\n      <ion-card>\n\n        <ion-row>\n\n          \n\n          <ion-col col-9>\n\n            <ion-row><h2 text-wrap>{{item.name}}</h2></ion-row>\n\n            <ion-row>{{item.brand}}</ion-row>\n\n            <!-- <ion-row>Size: {{item.sizes[0]}}</ion-row> -->\n\n            <ion-row>Price: ₹{{item.total}}</ion-row>\n\n            <ion-row>\n\n              <ion-col>\n\n                <button ion-button color="theme_dark" (click)="addItem(item)" icon-only>\n\n                <ion-icon name="ios-add-outline"></ion-icon>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col>\n\n                <ion-item><h2>{{item.quantity}}</h2></ion-item>\n\n              </ion-col>\n\n              <ion-col>\n\n                <button ion-button color="light" (click)="reduceItem(item)" icon-only>\n\n                <ion-icon name="ios-remove-outline"></ion-icon>\n\n                </button>\n\n              </ion-col>\n\n              <ion-col>\n\n                <button ion-button color="danger" (click)="deleteItem(i)" icon-only>\n\n                <ion-icon name="ios-trash-outline"></ion-icon>\n\n                </button>\n\n              </ion-col>\n\n            </ion-row>\n\n          </ion-col>\n\n\n\n          <ion-col col-3>\n\n            <img class="img-thumb" [src]="item.thumnail" />\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-row>\n\n  \n\n  </ion-content>\n\n  <ion-footer>\n\n    <ion-row>\n\n      <ion-col no-padding>\n\n        <button ion-button color="danger" class="footBtn" (click)="emptycart()">\n\n          Empty Cart\n\n        </button>\n\n      </ion-col>\n\n      <ion-col no-padding>\n\n        <button ion-button color="light" class="footBtn" (click)="toCheckout(items)">Checkout &nbsp;\n\n          <ion-icon class="btnIcon" name="md-paper-plane" ></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-footer>'/*ion-inline-end:"D:\Ionic app\src\pages\cart\cart.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cart_cart__["a" /* CartProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], CartPage);
    return CartPage;
}());

//# sourceMappingURL=cart.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_login_login__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, signupservice) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signupservice = signupservice;
        this.user = {};
        this.toggleBtn = false;
        this.typename = "password";
        this.selectOptions = {
            cssClass: 'gender-select',
            title: 'Choose gender'
        };
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.toggleEye = function () {
        console.log("PREV: " + this.toggleBtn);
        if (this.toggleBtn === true) {
            this.toggleBtn = false;
            this.typename = "password";
        }
        else {
            this.toggleBtn = true;
            this.typename = "text";
        }
    };
    SignupPage.prototype.register = function () {
        var _this = this;
        console.log("SignupUser: ", this.user);
        var dhruv;
        this.signupservice.getlogin(1, this.user).subscribe(function (res) {
            debugger;
            dhruv = res;
            _this.navCtrl.pop().then(function () { return _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]); });
        });
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"D:\Ionic app\src\pages\signup\signup.html"*/'<!--\n\n  Generated template for the SignupPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Signup</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content id="page_bg">\n\n    <div id="title_bg">\n\n    <div text-center style="font-size: 25vw; margin: 5vw; margin-bottom: 0vw; padding-top: 5vw;">\n\n        <img src="assets/imgs/theme_logo.png" />   \n\n    </div>\n\n    </div>\n\n\n\n    <div padding>\n\n      <form #signupForm="ngForm" (ngSubmit)="register()">\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-list>\n\n              <ion-item>\n\n                  <div item-start style="font-size: 5vw;"><ion-icon name="ios-person-outline"></ion-icon></div>\n\n                  <ion-input type="text" name="username" placeholder="UserName" id="usernameField" required [(ngModel)]="user.username"></ion-input>\n\n              </ion-item>\n\n\n\n              <ion-item>\n\n                  <div item-start style="font-size: 5vw;"><ion-icon name="ios-mail-outline"></ion-icon></div>\n\n                  <ion-input type="text" name="email" placeholder="Email" id="emailField" required [(ngModel)]="user.email"></ion-input>\n\n              </ion-item>\n\n\n\n\n\n              <ion-item>\n\n                  <div item-start style="font-size: 5vw;"><ion-icon name="ios-key-outline"></ion-icon></div>\n\n                  <ion-input [type]="typename" name="password" placeholder="Password" id="passwordField" required [(ngModel)]="user.password" ></ion-input>\n\n                  <button *ngIf="!toggleBtn" ion-button clear item-end style="font-size: 5vw;" color="dark" (click)="toggleEye()" icon-only><ion-icon name="ios-eye-off-outline"></ion-icon></button>\n\n                  <button *ngIf="toggleBtn" ion-button clear item-end style="font-size: 5vw;" (click)="toggleEye()" icon-only><ion-icon name="ios-eye-outline"></ion-icon></button>\n\n              </ion-item>\n\n\n\n            </ion-list>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <p text-wrap>(By signing up you agree to our terms & conditions & privacy policy.)</p>\n\n        </ion-row>\n\n\n\n        <ion-row>\n\n          <ion-col>\n\n            <button ion-button class="signup-button" type="submit" (click)="toggleEye()" [disabled]="!signupForm.form.valid">Signup</button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </form>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_login_login__["a" /* LoginProvider */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_product_product__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__details_details__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the SubPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SubPage = /** @class */ (function () {
    function SubPage(navCtrl, navParams, loadingCtrl, productService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.productService = productService;
        this.sizearray = [];
        this.colorarray = [];
        this.products = [];
        this.filtered = [];
        this.filteroption = false;
        this.prods = [];
        this.noproducts = false;
        this.selectedData = navParams.get('data');
        console.log("GOT DATA: ", this.selectedData);
        this.cat = this.selectedData.category;
        this.sub = this.selectedData.subcategory;
        this.getProducts(this.cat, this.sub);
    }
    ;
    ;
    SubPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SubPage');
    };
    SubPage.prototype.getProducts = function (cat, sub) {
        // this.loading = this.loadingCtrl.create({
        //   spinner: 'ios',
        //   content: "Loading..."
        // });
        var _this = this;
        // this.loading.present();
        var filter = { cat: this.cat, sub: this.sub };
        this.productService.getAllProducts(filter)
            .subscribe(function (data) {
            _this.products = data.products;
            if (_this.products.length == 0) {
                _this.noproducts = true;
            }
            console.log("SubPage: ", _this.products);
            _this.prods = _this.products;
            // this.hideLoading();
        }, function (error) {
            console.log("SubPage Error: ", error);
            // this.hideLoading();
        });
    };
    SubPage.prototype.Filter = function (call, tofilter, from) {
        // this.loading = this.loadingCtrl.create({
        //   spinner: 'ios',
        //   content: "Loading..."
        // });
        var _this = this;
        // this.loading.present();
        if (call == "filter") {
            if (from == 1) {
                this.filtercolor = '&filter[pa_color]=' + tofilter;
            }
            else if (from == 2) {
                this.filtersize = '&filter[pa_size]=' + tofilter;
            }
            var filter = { cat: this.cat, sub: this.sub, color: this.filtercolor, size: this.filtersize };
            this.productService.getAllProducts(filter)
                .subscribe(function (data) {
                _this.products = data.products;
                console.log("SubPage: ", _this.products);
                _this.prods = _this.products;
                // this.hideLoading();
            }, function (error) {
                console.log("SubPage Error: ", error);
                // this.hideLoading();
            });
        }
        else {
            this.filteroption = !this.filteroption;
            var self = this;
            this.productService.getAllAttirbutes().subscribe(function (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].name == "Color") {
                        _this.productService.getattributedetails(data[i].id).subscribe(function (data) {
                            self.colorarray = data;
                        }, function (error) {
                            console.log("SubPage Error: ", error);
                            // this.hideLoading();
                        });
                    }
                    else if (data[i].name == "Size") {
                        _this.productService.getattributedetails(data[i].id).subscribe(function (data) {
                            self.sizearray = data;
                        }, function (error) {
                            console.log("SubPage Error: ", error);
                            // this.hideLoading();
                        });
                    }
                }
                // this.hideLoading();
            }, function (error) {
                console.log("SubPage Error: ", error);
                // this.hideLoading();
            });
        }
    };
    SubPage.prototype.Sort = function () {
        // this.loading = this.loadingCtrl.create({
        //   spinner: 'ios',
        //   content: "Loading..."
        // });
        var _this = this;
        // this.loading.present();
        this.productService.getAllProducts(this.cat)
            .subscribe(function (data) {
            _this.products = data.products;
            console.log("SubPage: ", _this.products);
            _this.prods = _this.products;
            // this.hideLoading();
        }, function (error) {
            console.log("SubPage Error: ", error);
            // this.hideLoading();
        });
    };
    SubPage.prototype.hideLoading = function () {
        this.loading.dismiss();
    };
    SubPage.prototype.openDetails = function (product) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__details_details__["a" /* DetailsPage */], {
            data: product,
        });
    };
    SubPage.prototype.toCart = function () { this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]); };
    SubPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sub',template:/*ion-inline-start:"D:\Ionic app\src\pages\sub\sub.html"*/'<!--\n\n  Generated template for the SubPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>{{cat}} {{sub}}</ion-title>\n\n      <ion-buttons right>\n\n        <button ion-button icon-only>\n\n          <ion-icon name="md-search"></ion-icon>\n\n        </button>\n\n  \n\n        <button ion-button icon-only>\n\n          <ion-icon name="ios-bookmark-outline"></ion-icon>\n\n        </button>\n\n  \n\n        <button ion-button (click)="toCart()" icon-only>\n\n          <ion-icon name="ios-basket"></ion-icon>\n\n        </button>\n\n      </ion-buttons>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content  class="pageBack">\n\n\n\n    <ion-row class="items-wrapper"  *ngIf="!noproducts">\n\n      <ion-card class="itemThumb" *ngFor="let prod of prods">\n\n        <img class="imgThumb" [src]=\'prod.featured_src\' (click)="openDetails(prod)" />\n\n        <ion-card-content no-padding>\n\n          <p class="wrap-text brand-text">{{prod.brand}}</p> \n\n          <ion-row>\n\n            <ion-col class="price-text">₹{{prod.price}}</ion-col>\n\n          </ion-row>\n\n          <p class="wrap-text name-text">{{prod.title}}</p>\n\n        </ion-card-content>\n\n      </ion-card>\n\n      \n\n    </ion-row>\n\n  \n\n  </ion-content>\n\n  <ion-content  class="pageBack" *ngIf="noproducts">\n\n\n\n    <ion-row class="items-wrapper">\n\n      <ion-card class="itemThumb" >\n\n        <ion-card-content no-padding>\n\n          <p class="wrap-text brand-text">No Products!!</p> \n\n        </ion-card-content>\n\n      </ion-card>\n\n      \n\n    </ion-row>\n\n  \n\n  </ion-content>\n\n  \n\n  <ion-footer>\n\n      <ion-row *ngIf="filteroption">\n\n          <ion-card class="fullcard" >\n\n              <ion-card-content no-padding>\n\n                  <ion-list radio-group [(ngModel)]="filtercolor">\n\n                      <ion-list-header>Color</ion-list-header>\n\n                      <ion-scroll scrollY="true" style="height:17vw"> \n\n                      <ion-item *ngFor="let colors of colorarray" >\n\n                          <ion-label>{{colors.name}}</ion-label>\n\n                          <ion-radio value={{colors.name}} (click)="Filter(\'filter\',colors.name,1)"></ion-radio>\n\n                        </ion-item>\n\n                      </ion-scroll>\n\n                  </ion-list>\n\n                  <ion-list radio-group [(ngModel)]="filtersize">\n\n                      <ion-list-header>Size</ion-list-header>\n\n                      <ion-scroll scrollY="true" style="height:17vw">\n\n                      <ion-item *ngFor="let size of sizearray" >\n\n                          <ion-label>{{size.name}}</ion-label>\n\n                          <ion-radio value={{size.name}}></ion-radio>\n\n                        </ion-item>\n\n                      </ion-scroll>\n\n                  </ion-list>\n\n              </ion-card-content>\n\n            </ion-card>\n\n        </ion-row>\n\n    <ion-row>\n\n      <ion-col no-padding>\n\n        <button ion-button class="footBtn" (click)="Sort()">\n\n          <ion-icon class="btnIcon" name="ios-arrow-round-down"></ion-icon>\n\n          <ion-icon class="btnIcon" name="ios-arrow-round-up"></ion-icon>SORT\n\n        </button>\n\n      </ion-col>\n\n      <ion-col no-padding>\n\n        <button ion-button class="footBtn" (click)="Filter()">\n\n          <ion-icon class="btnIcon" name="ios-funnel" ></ion-icon>FILTER\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-footer>\n\n  '/*ion-inline-end:"D:\Ionic app\src\pages\sub\sub.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__providers_product_product__["a" /* ProductProvider */]])
    ], SubPage);
    return SubPage;
}());

//# sourceMappingURL=sub.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var OrderProvider = /** @class */ (function () {
    function OrderProvider(http) {
        this.http = http;
        console.log('Hello Orders Provider');
    }
    OrderProvider.prototype.getOrders = function (id) {
        return this.http.get(' https://undergarment.co.in/wc-api/v3/customers/' + id + '/orders?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27')
            .map(function (res) { return res; });
    };
    OrderProvider.prototype.createOrder = function (order) {
        return this.http.post('https://undergarment.co.in/wc-api/v3/orders?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27', order)
            .map(function (res) { return res; });
    };
    OrderProvider.prototype.ViewOrder = function (orderid) {
        return this.http.get('https://undergarment.co.in/wp-json/wc/v3/orders/' + orderid + '?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27')
            .map(function (res) { return res; });
    };
    OrderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
    ], OrderProvider);
    return OrderProvider;
    var _a;
}());

//# sourceMappingURL=orders.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LoginProvider = /** @class */ (function () {
    function LoginProvider(http) {
        this.http = http;
        this.myheader = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]();
        console.log('Hello Login Provider');
        this.myheader.append('Content-Type', 'application/json');
    }
    LoginProvider.prototype.getlogin = function (call, user) {
        debugger;
        if (call == 1) {
            return this.http.post('https://undergarment.co.in/wc-api/v3/customers?_method=POST&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27', {
                "customer": {
                    "email": user.email,
                    "password": user.password,
                    "first_name": "",
                    "last_name": "",
                    "username": user.username,
                    "billing_address": {
                        "first_name": " ",
                        "last_name": "",
                        "company": "",
                        "address_1": "",
                        "address_2": "",
                        "city": "",
                        "state": "",
                        "postcode": "",
                        "country": "",
                        "email": "",
                        "phone": ""
                    },
                    "shipping_address": {
                        "first_name": "",
                        "last_name": "",
                        "company": "",
                        "address_1": "",
                        "address_2": "",
                        "city": "",
                        "state": "",
                        "postcode": "",
                        "country": ""
                    }
                }
            }, { headers: this.myheader })
                .map(function (res) { return res; });
        }
        else if (call == 2) {
            return this.http.get('https://www.undergarment.co.in/api/auth/generate_auth_cookie/?username=' + user.email + '&password=' + user.password)
                .map(function (res) { return res; });
        }
    };
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], LoginProvider);
    return LoginProvider;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_home_home__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the MenuProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
        console.log('Hello MenuProvider Provider');
    }
    MenuProvider.prototype.getMenu = function () {
        return this.http.get('assets/data/menu.json')
            .map(function (res) { return res; });
    };
    MenuProvider.prototype.getCategory = function () {
        return this.http.get('assets/data/categories.json')
            .map(function (res) { return res; });
    };
    MenuProvider.prototype.getMenu2 = function () {
        this.pages = [
            { title: 'Orders', component: __WEBPACK_IMPORTED_MODULE_3__pages_home_home__["a" /* HomePage */], icon: 'ios-filing-outline' },
            { title: 'T&C', component: __WEBPACK_IMPORTED_MODULE_3__pages_home_home__["a" /* HomePage */], icon: 'ios-basket-outline' },
            { title: 'Return Policy', component: __WEBPACK_IMPORTED_MODULE_3__pages_home_home__["a" /* HomePage */], icon: 'ios-heart-outline' }
        ];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_product_product__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__category_category__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cart_cart__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, productService, storage) {
        this.navCtrl = navCtrl;
        this.productService = productService;
        this.storage = storage;
        this.showsearch = false;
        this.items = [];
        this.slideImages = [
            { title: "SHORTS OFFER", url: "./assets/imgs/slides/slid1.jpeg" },
            { title: "SHORTS OFFER", url: "./assets/imgs/slides/slid2.jpeg" },
            { title: "SHORTS OFFER", url: "./assets/imgs/slides/slid3.jpeg" },
        ];
        this.categorys = [
            { title: "MEN'S", key: "Men", url: "./assets/imgs/trends/men.jpg" },
            { title: "WOMEN'S", key: "Women", url: "./assets/imgs/trends/women.jpg" },
            { title: "BOY'S", key: "Boys", url: "./assets/imgs/trends/boys.jpg" },
            { title: "GIRL'S", key: "Girls", url: "./assets/imgs/trends/girl.jpg" },
        ];
        this.getArrivalsData();
        this.getCartItems();
    }
    HomePage.prototype.Show = function () {
        this.showsearch = !this.showsearch;
    };
    HomePage.prototype.getArrivalsData = function () {
        var _this = this;
        this.productService.getArrivals()
            .subscribe(function (res) {
            _this.arrivals = res;
            console.log("ARRIVALS", _this.arrivals);
        });
    };
    HomePage.prototype.toCategory = function (cat) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__category_category__["a" /* CategoryPage */], {
            data: cat
        });
    };
    HomePage.prototype.toCart = function () { this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__cart_cart__["a" /* CartPage */]); };
    HomePage.prototype.getCartItems = function () {
        var _this = this;
        this.storage.get('item').then(function (val) {
            _this.items = val;
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"D:\Ionic app\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>\n\n      Mangali Hosiery\n\n    </ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button (click)="Show()" icon-only>\n\n        <ion-icon  name="md-search"></ion-icon>\n\n      </button>\n\n\n\n      <button ion-button  (click)="toCart()" icon-only>\n\n        <ion-icon name="ios-bookmark-outline"></ion-icon>\n\n      </button>\n\n\n\n      <button ion-button (click)="toCart()" icon-only>\n\n        <ion-icon name="ios-basket"><ion-badge color="theme_dark">{{items.length}}</ion-badge></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-searchbar\n\n  *ngIf="this.showsearch"\n\n  [(ngModel)]="myInput"\n\n  [showCancelButton]="shouldShowCancel"\n\n \n\n  (ionCancel)="onCancel($event)">\n\n</ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-slides class="bannersThumb1" pager>\n\n    <ion-slide *ngFor="let slideImage of slideImages">\n\n      <img class="bannersThumb1" [src]=\'slideImage.url\'/>     \n\n    </ion-slide>\n\n  </ion-slides>\n\n\n\n  <ion-title><div class="title-line"><span>New Arrivals</span></div></ion-title>\n\n\n\n  <ion-slides class="arrivalThumb">\n\n    <ion-slide *ngFor="let arrival of arrivals">\n\n    \n\n    <ion-col col-9 class="item-wrapper">\n\n    <ion-card ion-col class="itemThumb">\n\n      <img class="imgThumb" [src]=\'arrival.thumbnail\' />\n\n      <ion-card-content no-padding>\n\n        <p class="wrap-text brand-text">{{arrival.brand}}</p> \n\n        <ion-row>\n\n          <ion-col class="price-text">₹{{arrival.selling_price}}</ion-col>\n\n        </ion-row>\n\n        <p class="wrap-text name-text">{{arrival.name}}</p>\n\n      </ion-card-content>\n\n    </ion-card>\n\n    </ion-col>\n\n  </ion-slide>\n\n  </ion-slides>\n\n\n\n  <ion-title><div class="title-line"><span>Shop</span></div></ion-title>\n\n\n\n  <ion-row class="trendingDiv">\n\n    <ion-thumbnail *ngFor="let c of categorys" (click)="toCategory(c.key)">\n\n      <div>\n\n        <img class="trendingThumb" [src]=\'c.url\' />\n\n      </div>\n\n      <div class="trendingCentered">{{c.title}}</div>\n\n    </ion-thumbnail>\n\n  </ion-row>\n\n\n\n  <ion-item-divider text-center padding>\n\n    <p>Mangli Hosiery &#169; </p>\n\n  </ion-item-divider>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Ionic app\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_product_product__["a" /* ProductProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ProductProvider = /** @class */ (function () {
    function ProductProvider(http) {
        this.http = http;
        this.products = [];
        console.log('Hello ProductProvider Provider');
    }
    ProductProvider.prototype.getArrivals = function () {
        return this.http.get('assets/data/arrivals-data.json')
            .map(function (res) { return res; });
    };
    ProductProvider.prototype.getAllProducts = function (filter) {
        if (filter.color || filter.size) {
            if (filter.color && filter.size) {
                return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]=' + filter.cat + filter.color + filter.size + '&filter[category]=' + filter.sub + '&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map(function (res) { return res; });
            }
            else if (filter.color) {
                return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]=' + filter.cat + filter.color + '&filter[category]=' + filter.sub + '&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map(function (res) { return res; });
            }
            else if (filter.size) {
                return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]=' + filter.cat + '&filter[category]=' + filter.sub + filter.size + '&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map(function (res) { return res; });
            }
        }
        else {
            return this.http.get('https://undergarment.co.in/wc-api/v3/products?filter[category]=' + filter.cat + '&filter[category]=' + filter.sub + '&consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27').map(function (res) { return res; });
        }
    };
    ProductProvider.prototype.getAllAttirbutes = function () {
        return this.http.get('https://undergarment.co.in/wp-json/wc/v3/products/attributes?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27')
            .map(function (res) { return res; });
    };
    ProductProvider.prototype.getattributedetails = function (id) {
        return this.http.get('https://undergarment.co.in/wp-json/wc/v3/products/attributes/' + id + '/terms?consumer_key=ck_cadbf486deac27501ce872892f7af2a7ddff9032&consumer_secret=cs_08412432d72007a20147097208297ed6be18db27')
            .map(function (res) { return res; });
    };
    ProductProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ProductProvider);
    return ProductProvider;
}());

//# sourceMappingURL=product.js.map

/***/ })

},[226]);
//# sourceMappingURL=main.js.map